import os
from django.conf import settings


import sys
sys.path.append(settings.ENDPOINT)
import Services, Nodes, Sellers, Customers,Plans

import numpy as np 
import pandas as pd 


#====================================================================#
#====================== SEPARATE NUMBER =============================#
#====================================================================#
def thounsand_separator(number):
	"""
	SEPARA LOS NUMEROS CON PUNTOS A PARTIR DE 1000 EJ.(1.000)
	"""

	return ("{:,}".format(number).replace(',','.')) 

#====================================================================#
#=================== CONVERTIDOR DE FECHAS ==========================#
#====================================================================#
def convert_date(date):
	"""
	FUNCION QUE TOMA UN STRING Y LO CONVIERTE EN UNA FECHA
	YYYY-mm-dd H:MM:SS y la retorna con el formato YYYY-mm-dd
	"""
	formato = "%Y-%m-%d"
	return pd.to_datetime(date,format=formato).strftime(formato)

#====================================================================#
#====================== CLAVES DATAFRAME ============================#
#====================================================================#
def get_claves(df):
	"""
	Genera las claves del diccionario de un dataframe
	"""
	output = list()
	for key, value in df.to_dict().items():
		for k,v in value.items():
			output.append(k)

	return set(output)


#====================================================================#
#====================== FECHAS DATAFRAME ============================#
#====================================================================#

def fechas_df(x):
	return pd.to_datetime(x,format="%Y-%m-%d")

#=================================================================================================#
#======================= Variables de disponibles para todos las clases #=========================#
#=================================================================================================#


contracts_request = dict(
	plan = ['service_id','service_name','service_active'],
	nodo = ['node_id','node_code'],
	vendedor = ['seller_id','seller_name'],
	comuna = ['commune'],
	documento = ['document_type'],
	status = ['get_status_display'],
	cliente = ['customer_name','customer_rut','number'],
	tecnologia = ['get_technology_kind_display'],
	)

dataframe_key = dict(
	plan = 'service_name',
	nodo = 'node_code',
	vendedor = 'seller_name',
	comuna = 'commune',
	documento = 'document_type',
	status = 'get_status_display',
	)

meses = {
	'1':'Enero',
	'2':'Febrero',
	'3':'Marzo',
	'4':'Abril',
	'5':'Mayo',
	'6':'Junio',
	'7':'Julio',
	'8':'Agosto',
	'9':'Septiembre',
	'10':'Octubre',
	'11':'Noviembre',
	'12':'Diciembre'
}

###################################################################################################
######################################## DATAFRAME ################################################
###################################################################################################


def get_dataframe_services():

	dataframe = pd.DataFrame(Services.services,
							columns=['id','get_status_display','service_name','service_price',
							'service_id','commune','node_code','customer_name','node_id',
							'customer_rut','number','activated_on','service_active',
							'seller_name','get_technology_kind_display','seller_id',
							'document_type','composite_address'])\
					.fillna('2001-01-01')

	dataframe['activated_on'] = pd.to_datetime(dataframe.activated_on, 
									format="%Y-%m-%d")\
									.apply(lambda x:x.strftime('%Y-%m-%d'))

	dataframe['annio'] = pd.to_datetime(dataframe.activated_on).dt.year
	dataframe['mes'] = pd.to_datetime(dataframe.activated_on).dt.month
	dataframe['mes_calendario'] = dataframe.mes.apply(lambda x:meses[str(x)])
	dataframe['trimestre'] = pd.to_datetime(dataframe.activated_on).dt.quarter
	dataframe['semestre'] = np.where(dataframe['trimestre'].isin([1,2]),1,2)

	return dataframe


def get_dataframe_customers():

	dataframe = pd.DataFrame(Customers.customers,
							columns=['id','name','rut','commune'])\
					.fillna('2001-01-01')

	dataframe['customer_id'] = dataframe['id']

	return dataframe