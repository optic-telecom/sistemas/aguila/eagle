
# request http
import requests

#============================================================#
#====================== REQUESTS ============================#
#============================================================#

def requestBasico(endpoint):
    headers = {'Authorization' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }
    cadena = endpoint.lower()
    url = 'https://190.113.247.198/api/v1/{}/'.format(cadena)
    response = requests.get(url, headers=headers, verify=False)
    results = response.json()['results']

    return results

def get_data_url(url):

    headers = {
    	'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }

    COUNT = requests.get(
        url=url,
        headers = headers,
        verify=False
    ).json()['count']

    return requests.get(
        url=url,
        params={'limit': COUNT, 'offset': 0},
        headers = headers,
        verify=False
    ).json()['results']

#============================================================#
#================== VARIABLES GLOBALES ======================#
#============================================================#


