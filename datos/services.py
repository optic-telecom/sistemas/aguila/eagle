from django.conf import settings
import os
### Importación de archivos externos al py águila(IMPORTANTE)
import sys
sys.path.append(settings.ENDPOINT)
sys.path.append(settings.MIX)
import Plans,Customers,Nodes,Sellers,Services,Users

# from endpoint.Crudos import (Contracts,Customers,Nodes,Sales,Sellers,Services,Users)

import pandas as pd 
# import numpy as np
# from scipy import stats
# import requests


class GeneralContractsSales():

	def get_claves(self, df):
		out = []

		for key,values in df.to_dict().items():
			for k,v in values.items():
				out.append(k)

		return set(out)

	def get_contracts(self):
		datos = Services.services
		services = Plans.plans
		nodes = Nodes.nodes
		formato = '%Y-%m-%d'

		for dat in datos:
			dat['customer_id'] = int(dat['customer'].split('/')[6])
			dat['service_id'] = int(dat['service'].split('/')[6])
			dat['node_id'] = int(dat['node'].split('/')[6])

			if dat['seller']:
				dat['seller_id'] = int(dat['seller'].split('/')[6])
			else:
				dat['seller_id'] = 11


		for contract in datos:
			for service in services:
				if contract['service_id'] == service['id']:
					contract['service_price'] = float(service['price'])
					contract['service_name'] = service['name']
					contract['service_active'] = service['active']

		for contract in datos:
			for node in nodes:
				if contract['node_id'] == node['id']:
					contract['node_code'] = node['code']

		customers = Customers.customers

		for contract in datos:
			for customer in customers:
				if contract['customer_id'] == customer['id']:
					contract['customer_name'] = customer['name']
					contract['customer_rut'] = customer['rut']

		for contract in datos:
			for seller in Sellers.sellers:
				if contract['seller_id'] == seller['id']:
					contract['seller_name'] = seller['name']


		for contract in datos:
			if contract['activated_on']:
				contract['activated_on'] = contract['activated_on'].split('T')[0]
			else:
				contract['activated_on'] = '2001-01-01'
			del contract['customer']
			del contract['service']
			del contract['node']
			del contract['seller']

		try:

			with open(settings.ENDPOINT + '/Services.py', 'w') as File:
				File.write('services = {}'.format(datos))


		except Exception as e:


			with open(settings.MIX + '/Services.py', 'w') as File:
				File.write('services = {}'.format(datos))

			with open('/home/avargas/Escritorio/endpoint/Crudos/Services.py', 'w') as File:
				File.write('services = {}'.format(datos))



def ejecutaContract():
	c = GeneralContractsSales()
	c.get_contracts()




