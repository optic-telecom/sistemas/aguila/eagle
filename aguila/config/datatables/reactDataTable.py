import numpy as np
import pandas as pd
from django.http import JsonResponse
from django.templatetags.i18n import language
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from nbformat import ValidationError
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from config.utils.dataframes import (get_claves, get_dataframe_services, meses,
                                     thousand_separator)
from customers.models import Servicio
from customers.serializers import ServicesSz, ServicioSerializer


@method_decorator(csrf_exempt,name="dispatch")
class DataTableObjects(APIView):
    renderer_classes = [JSONRenderer,]
    columns = []
    order = []
    palabraClave = 'registros'
    serializer_class = ServicioSerializer
    button = True

    def post(self,request,format='json'):
        qs = self.get_data()

        datatable = dict(
            columns = self.get_columns(),
            destroy = True,
            data = self.serializer_class(qs,many=True).data,
            order = [self.order],
            responsive = True,
            count = np.size(qs),
            language = self.language(),
            button = self.button,

        )
        return Response(datatable,status=200)

    def get_data(self):
        qs = Servicio.objects.all()
        startDate = self.request.data.get('startDate','2019-01-01')
        endDate = self.request.data.get('endDate','2019-06-30')

        seleccionA = self.request.data.get('seleccionA','plan')
        seleccionB = self.request.data.get('seleccionB','status')
        seleccionC = self.request.data.get('seleccionC','comuna')

        eleccionA = self.request.data.get('eleccionA','["2018 - Fibra - Internet 30Megas","2018 - Fibra - Internet 150Megas"]')
        eleccionB = self.request.data.get('eleccionB','["activo","moroso"]')
        eleccionC = self.request.data.get('eleccionC','["Santiago","Arica","Estación Central"]')



        filtro = {
            'fecha_recaudacion__range' : [startDate,endDate],
            seleccionA+'__in' : eval(eleccionA),
            seleccionB+'__in' : eval(eleccionB),
            seleccionC+'__in' : eval(eleccionC),

        }
        if startDate and endDate:
            qs = qs.filter(**filtro)
        
        qs = list(qs.values().order_by('-nro_servicios'))


        df = pd.DataFrame(qs)
        df = df.groupby([seleccionA,seleccionB,seleccionC]).agg({
            'nro_servicios':[np.sum],
            'recaudacion':[np.sum],
            'promedio':[np.mean]
        })


        output = list()
        for clave in get_claves(df):
            output.append(self.list_data(clave,df))



        return output

    def list_data(self,clave,df):
        return {
                'seleccionA':clave[0],
                'seleccionB':clave[1],
                'seleccionC':clave[2],
                'nro_servicios':df['nro_servicios']['sum'][clave],
                'recaudacion':df['recaudacion']['sum'][clave],
                'promedio':df['promedio']['mean'][clave],
            }


    def get_columns(self):
        columnas = self.columns
        output = list()
        for col in columnas:
            if col in ['nro_contrato','nro_servicios','recaudacion','promedio']:
                output.append(
                    {
                        'data':col,'className':'text-center','font':'italic'
                    }
                )
            else:
                output.append({
                    'data':col,
                    'className':'text-nowrap','font-style':'italic'
                })
        
        if self.button:
            output.append({
                'data':None
            })
        return output

    def language(self):
        spanish = {
            "decimal": ",",
            "thousands": ".",
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ " + self.palabraClave,
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando " + self.palabraClave + " del _START_ al _END_ de un total de _TOTAL_ " + self.palabraClave,
            "sInfoEmpty":      "Mostrando " + self.palabraClave + " del 0 al 0 de un total de 0 " + self.palabraClave,
            "sInfoFiltered":   "(filtrado de un total de _MAX_ " + self.palabraClave + ")",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            'buttons': {
            'copy': "Copiar",
            'print':'Imprimir',
            'colvis':'Columnas Visibles',
            }
        
        }
        return spanish
