"""aguila URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.contrib import admin
from django.urls import include, path

from general.views2 import SwaggerSchemaView


from users.urls import router as users_router
from users.urls import urlpatterns as urlpatterns_user

urlpatterns = [
    path('api/docs/', SwaggerSchemaView.as_view()),
    path('admin/', admin.site.urls),
    

    path('',include('general.urls',namespace="general")),

    path('api/v1/',include('customers.urls',namespace="customers")),
    path('api/v1/',include('finances.urls',namespace="finances")),

    path('api/', include(users_router.urls))

] + urlpatterns_user

if os.environ.get('ENTORNO') == 'local':


    urlpatterns += [
        path('pruebas/', include('pruebas.urls',namespace='pruebas')),
    ]
