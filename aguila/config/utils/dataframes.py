

from Endpoint.Crudos import (Nodes, Sellers, Customers,Plans)
from Endpoint.Mix import (Services)

import numpy as np 
import pandas as pd 



#====================================================================#
#====================== SEPARATE NUMBER =============================#
#====================================================================#
def thousand_separator(number):
	"""
	SEPARA LOS NUMEROS CON PUNTOS A PARTIR DE 1000 EJ.(1.000)
	"""

	return ("{:,}".format(int(number)).replace(',','.')) 

#====================================================================#
#=================== CONVERTIDOR DE FECHAS ==========================#
#====================================================================#
def convert_date(date):
	"""
	FUNCION QUE TOMA UN STRING Y LO CONVIERTE EN UNA FECHA
	YYYY-mm-dd H:MM:SS y la retorna con el formato YYYY-mm-dd
	"""
	formato = "%Y-%m-%d"
	return pd.to_datetime(date,format=formato).strftime(formato)

#====================================================================#
#====================== CLAVES DATAFRAME ============================#
#====================================================================#
def get_claves(df):
	"""
	Genera las claves del diccionario de un dataframe
	"""
	output = list()
	for key, value in df.to_dict().items():
		for k,v in value.items():
			output.append(k)

	return set(output)


#====================================================================#
#====================== FECHAS DATAFRAME ============================#
#====================================================================#

def fechas_df(x):
	return pd.to_datetime(x,format="%Y-%m-%d")

#=================================================================================================#
#======================= Variables de disponibles para todos las clases #=========================#
#=================================================================================================#


meses = {
	'1':'Enero',
	'2':'Febrero',
	'3':'Marzo',
	'4':'Abril',
	'5':'Mayo',
	'6':'Junio',
	'7':'Julio',
	'8':'Agosto',
	'9':'Septiembre',
	'10':'Octubre',
	'11':'Noviembre',
	'12':'Diciembre'
}

###################################################################################################
######################################## DATAFRAME ################################################
###################################################################################################


def get_dataframe_services(**kwargs):

	dataframe = pd.DataFrame(Services.services,
							columns=['id','get_status_display','plan_name','plan_price',
							'plan_id','commune','node_code','customer_name','node_id',
							'customer_rut','number','activated_on','plan_active',
							'seller_name','get_technology_kind_display','seller_id',
							'document_type','composite_address'])\
					.fillna('2001-01-01')

	dataframe['activated_on'] = pd.to_datetime(dataframe.activated_on, 
									format="%Y-%m-%d")\
									.apply(lambda x:x.strftime('%Y-%m-%d'))

	dataframe['annio'] = pd.to_datetime(dataframe.activated_on).dt.year
	dataframe['mes'] = pd.to_datetime(dataframe.activated_on).dt.month
	dataframe['mes_calendario'] = dataframe.mes.apply(lambda x:meses[str(x)])
	dataframe['semana'] = pd.to_datetime(dataframe.activated_on).dt.week
	dataframe['trimestre'] = pd.to_datetime(dataframe.activated_on).dt.quarter
	dataframe['semestre'] = np.where(dataframe['trimestre'].isin([1,2]),1,2)

	if 'startDate' in kwargs and 'endDate' in kwargs:
		startDate = kwargs['startDate']
		endDate = kwargs['endDate']
		mask = (dataframe['activated_on'] >= startDate)&(dataframe['activated_on'] <= endDate)
		dataframe = dataframe[mask]

	return dataframe


def get_dataframe_customers():

	dataframe = pd.DataFrame(Customers.customers,
							columns=['id','name','rut','commune'])\
					.fillna('2001-01-01')

	dataframe['customer_id'] = dataframe['id']

	return dataframe