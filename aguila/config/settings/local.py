from .base import *

INSTALLED_APPS += [
    'pruebas',
]

MIDDLEWARE += [
    
]


REST_FRAMEWORK = {
    # 'DEFAULT_PERMISSION_CLASSES': (
    #     'rest_framework.permissions.IsAuthenticated',#  AllowAny
    # ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': [
        'url_filter.integrations.drf.DjangoFilterBackend',
    ],    
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',    
    'DEFAULT_PAGINATION_CLASS': 'config.utils.pagination.LinkHeaderPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ),
    'NUM_PROXIES': 2,
    'DEFAULT_THROTTLE_RATES': {
        'anon': '100/hour',
        'user': '700/hour'
    },
    #token
    'JWT_EXPIRATION_DELTA': timedelta(hours=48),
    "JWT_ALLOW_REFRESH": True,

}


TIME_ZONE = 'America/Caracas'