from django.urls import path
from mixins.MixinJWT import JWTView
from django.views.generic import TemplateView
from users.views import (javascript,UserViewSet, UserView, PasswordReset, HomeAPIView)
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'api/users', UserViewSet)

app_name='users'

urlpatterns = [
    path('api/v1/user', UserView.as_view(), name='user_view'), 

    #====================================> USUARIOS <====================================
    # JAVASCRIPTS
    path('javascript.js', javascript, name='js'),

    # LOGIN

    path('login',
        TemplateView.as_view(template_name="users/registration/login.html"),
        name='login'),
    
    # HOME

    path('',
        JWTView.as_view(template_name='base/base.html'),
        name='home'),# AQUI CARGA LOS CSS Y JS

    path('home',
        JWTView.as_view(template_name='users/index.html'),
        name='index'), # DESDE AQUÍ USA LA CACHÉ CON AJAX


    path('api/v1/home-api', HomeAPIView.as_view(), name='home-api'),
    

    #====================================> CLIENTES <====================================
    
    # DINAMICA

    path('clientes/general',
    	JWTView.as_view(template_name='customers/dinamic/dinamic.html'),
    	name='customers_dinamic_view'),


    #====================================> FINANZAS <====================================

    #RECAUDACION
    path('finanzas/recaudacion',
        JWTView.as_view(template_name="finances/recaudacion/recaudacion.html"),
        name="finances_recaudacion_view"),

    #COMPARAR PAGOS
    path('finanzas/comparar-pagos',
        JWTView.as_view(
            template_name="finances/facturacion-recaudacion/comparar_pagos.html"
            ),
        name="comparar_pagos"),

    #FACTURACION
    path('finanzas/facturacion',
        JWTView.as_view(
            template_name="finances/facturacion/facturacion_view.html"
            ),
        name="facturacion_view"),

    #====================================> CLIENTES <====================================



]