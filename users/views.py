from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse

from django.conf import settings

import json

from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.generics import RetrieveAPIView, UpdateAPIView, CreateAPIView, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer, CreateUserSerializer,PasswordResetSerializer

User = get_user_model()

from django.utils import timezone
from datetime import date
from dateutil.relativedelta import relativedelta
# Create your views here.


import sys
sys.path.append(settings.ENDPOINT)
import Services, Nodes, Sellers, Plans, Customers

from datos.dataframes import get_dataframe_services,get_claves,meses

import numpy as np 
import pandas as pd



def javascript(request):
    """
    Vista que me permite acceder a variables javascript de manera global dentro
    del proyecto aguila
    """

    hoy = date.today()
    contexto = dict(

        fecha = hoy.strftime('%d-%m-%Y'),
        version = 'v 0.2.0',
        app_name = 'Águila',
        company = 'Optic',
        slogan = 'Sistema para la generación de reportes estadísticos de avanzada.',
        annio = str(hoy.year),
        name_jwt = settings.NAME_JWT,     

    )

    http_response = 'var Django = ' + json.dumps(contexto, indent=1)
    return HttpResponse(http_response, content_type='application/javascript')



class UserViewSet(viewsets.ModelViewSet):
    """
    create:
        Create a user
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method.lower() == "post":
            return CreateUserSerializer
        return super(UserViewSet, self).get_serializer_class()

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [AllowAny()]
        return super(UserViewSet, self).get_permissions()


class UserView(RetrieveAPIView, UpdateAPIView):
    """
    get:
        Connected user information

    put:
        Update user information.

    pacth:
        Update user information.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)


    def get_object(self):
        return self.request.user


class PasswordReset(CreateAPIView):
    """
    post:
        End point for password change

    """
    serializer_class = PasswordResetSerializer


#======================================================================#
#=========================== API HOME EAGLE ===========================#
#======================================================================#

class HomeAPIView(APIView):
    """
    Api quee esta destinada a brindar en el home una serie de
    estadisticas de diferentes endpoints

    """

    def get(self, request):

        hoy = date.today()
        mes_pasado = date(hoy.year,1,1)
        
        hoy = hoy.strftime("%Y-%m-%d")
        mes_pasado = mes_pasado.strftime("%Y-%m-%d")



        startDate = self.request.GET.get('startDate',mes_pasado)
        endDate = self.request.GET.get('endDate',hoy)
        try:
            contexto = dict(
                startDate = startDate,
                endDate = endDate
                )
            json = self.get_services_last_30_days(**contexto)

            return Response(json)
        except Exception as e:
            return Response({
                'results':False,
                'errorMsj':'No se ha podido completar su solicitud'
            })

    def get_services_last_30_days(self,**kwargs):
        """
        Estadistica de los últimos 30 dias de los contratos generados
        """
        formato = "%d-%m-%Y"
        formato_salida = "%Y-%m-%d"


        startDate = pd.to_datetime(kwargs['startDate'], format=formato_salida)\
                        .strftime(formato_salida)
        endDate = pd.to_datetime(kwargs['endDate'], format=formato_salida)\
                        .strftime(formato_salida)

        df = get_dataframe_services()

        df['activated_on'] = pd.to_datetime(df.activated_on, 
                                            format=formato_salida)\
                                .apply(lambda x:x.strftime(formato_salida))

        df['annio'] = pd.to_datetime(df.activated_on,format=formato_salida).dt.year
        df['mes_id'] = pd.to_datetime(df.activated_on,format=formato_salida).dt.month
        df['mes_calendario'] = df.mes_id.apply(lambda x:meses[str(x)])

        #===== FILTRO =====#
        mask = (df['activated_on']>=startDate)&(df['activated_on']<=endDate)
        df = df[mask]
        df = df[df['get_status_display'].isin(['activo','moroso',
                                            'retirado','por retirar'])]

        if np.size(df['id']) > 0:
            #===== COMUNAS =====#
            comunas_list = df['commune'].value_counts().to_dict().items()
            comunas_list = [{'comuna':key,'nro_servicios':value} for key, value in comunas_list]
            comunas_list = sorted(comunas_list, key=lambda x:[-x['nro_servicios']])
            #===== NODOS =====#
            nodos_list = df['node_code'].value_counts().to_dict().items()
            nodos_list = [{'nodo':key,'nro_servicios':value} for key, value in nodos_list]
            nodos_list = sorted(nodos_list, key=lambda x:[-x['nro_servicios']])

            df1 = df.groupby(['get_status_display']).service_price.agg([np.size,np.sum])
            df1 = df1.sort_values(by=['size'],ascending=False)

            # df2 = df.groupby(['get_status_display','annio','mes_id','mes_calendario','activated_on']).service_price.agg([np.size,np.sum])
            # df2 = df2.sort_values(by=['mes_id','activated_on','size'],ascending=False)

            # print(df2)

            color = dict(
                activo = '#00695c',
                retirado = '#c62828',
                por_retirar = '#ffeb3b',
                moroso = '#9e9e9e'
                )

            output = list()

            for clave in get_claves(df1):
                porcentaje = (df1['size'][clave]/df1['size'].sum())*100,
                output.append({
                    'status':clave.upper(),
                    'nro_servicios':df1['size'][clave],
                    'recaudacion':df1['sum'][clave],
                    'porcentaje':np.where(porcentaje,porcentaje,0)[0],
                    'color':color[clave.replace(' ','_')]
                    })

            output = sorted(output,key=lambda x:[-x['nro_servicios']])

            kwargs['startDate'] = pd.to_datetime(kwargs['startDate']).strftime(formato)
            kwargs['endDate'] = pd.to_datetime(kwargs['endDate']).strftime(formato)
            salida = dict(
                results = True,
                status = output,
                rango = kwargs,
                comunas = comunas_list,
                nodos = nodos_list,
                )
        else:
            salida = dict(

                results = False,
                errorMsj = 'No hay información para mostrar'
                )

        return salida





