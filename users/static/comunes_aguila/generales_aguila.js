//=========== LETRAS CAPITALES ===============//
function Capitalize(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//=========== NUMEROS SEPARADOS POR PUNTOS Y COMMAS ===============//

function commaSeparateNumber(val){
    // Separa con puntos y como los números en el datatable

    val = val.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return '<b><i>'+val+'</i></b>';
}

function SeparateNumber(val, param = 'entero'){
    // Separa con puntos y como los números en el datatable

    if(param != 'entero'){
        decimal = 2
    }else{
        decimal = 0
    }

    val = val.toFixed(decimal).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return val
}

//=========== CARGA DE SELECTORES CON DATA PROVENIENTE DEL SERVIDOR ===============//

function get_selector_charge(data,clave,id){
  $(id).select2({
    'width':'100%',
    'templateSelection': function (data, container) {
      $(".select2-selection__choice__remove").css({
        'color':'red',
      })
      $(container).css({
        "background-color":'#1e88e5',
        "color":'white'
      })
      return data.text
    },

  });

  selector = '<option value="todos">..:: '+clave+' ::..</option>'

  for(var i=0;i<data.length;i++){
    selector += '<option value="'+data[i]+'">'+data[i]+'</option>'
  }
  $(id).html(selector)

}

var jsonDataRange = {
      format: 'YYYY-MM-DD',
      applyClass: 'btn-warning',
      cancelClass: 'btn-inverse',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Fecha',
        daysOfWeek: [
        "Lun",
        "Mar",
        "Mié",
        "Jue",
        "Vie",
        "Sáb",
        "Dom"],
        monthNames: [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        firstDay: 0
      }
    }

//=========== MANEJO DE FECHAS JS ===============//

var FechaJs = function(date) {
  var currentDate = new Date(date);
  var dd = currentDate.getDate();
  var mm = currentDate.getMonth() + 1;
  var yyyy = currentDate.getFullYear();

  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  currentDate = dd+'-'+mm+'-'+yyyy

  return currentDate;
};


json_select2 = {
  'width':'100%',
    'language': {
      errorLoading:function(){
          return Capitalize(HDD.get('username')) + ", no se pudieron cargar los resultados"
      },
      inputTooLong:function(e){
          var n=e.input.length-e.maximum,r="Por favor, elimine "+n+" car";return r+=1==n?"ácter":"acteres"
      },
      inputTooShort:function(e){
          var n=e.minimum-e.input.length,r="Por favor, introduzca "+n+" car";return r+=1==n?"ácter":"acteres"
      },
      loadingMore:function(){
          return Capitalize(HDD.get('username')) + ", estamos cargando más resultados…"
      },
      maximumSelected:function(e){
          var n=Capitalize(HDD.get('username')) + ", sólo puede seleccionar "+e.maximum+" elemento";
          return 1!=e.maximum&&(n+="s"),n
      },
      noResults:function(){
          return Capitalize(HDD.get('username')) + ", no se encontraron resultados"
      },
      searching:function(){
          return Capitalize(HDD.get('username')) + ", estamos buscando…"
      },
      removeAllItems:function(){
          return"Eliminar todos los elementos"
      }
  },

}

var language_dt = function(palabraClave){

    language = {
      "decimal": ",",
      "thousands": ".",
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ " + palabraClave,
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando " + palabraClave + " del _START_ al _END_ de un total de _TOTAL_ " + palabraClave,
      "sInfoEmpty":      "Mostrando " + palabraClave + " del 0 al 0 de un total de 0 " + palabraClave,
      "sInfoFiltered":   "(filtrado de un total de _MAX_ " + palabraClave + ")",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      },
      'buttons': {
      'copy': "Copiar",
      'print':'Imprimir',
      'colvis':'Columnas Visibles',
      },
  }

  return language
}