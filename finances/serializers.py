from rest_framework import serializers


class AbstractNumsSerializers(serializers.Serializer):
	nro_servicios = serializers.IntegerField()
	recaudacion = serializers.FloatField()
	promedio = serializers.FloatField()


class RecaudacionSerializers(serializers.Serializer):
	tipo_pago = serializers.CharField(max_length=50)
	nro_servicios = serializers.IntegerField()
	recaudacion = serializers.FloatField()
	botones = serializers.SerializerMethodField()

	def get_botones(self, obj):
		botones = '<center>'
		botones += '<a onclick="return filtro_chart({0});" class="btn btn-xs btn-info" title="Detalle de {1}"><i class="fa fa-chart-line"></i></a>'.format(obj,obj['tipo_pago'])
		botones += '</center>'
		return botones