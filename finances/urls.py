from django.urls import path
from django.views.generic import TemplateView
from finances.views import (RecaudacionAPIView,FacturacionVsRecaudacionAPIView,
							FacturaAPIView, IClassAPIView)

app_name='finances'

urlpatterns = [
	
	#recaudacion

	path('recaudacion-api', 
		RecaudacionAPIView.as_view(), 
		name="recaudacion-api"),

	#facturacion vs recaudacion

	path('facturacion-recaudacion-api',
		FacturacionVsRecaudacionAPIView.as_view(),
		name="facturacion-recaudacion-api"),

	#facturas
	path('facturacion-api', 
		FacturaAPIView.as_view(),
		name="facturacion-api"),

	#ICLASS
	path('iclass-os', IClassAPIView.as_view(),name='iclass-os')

]