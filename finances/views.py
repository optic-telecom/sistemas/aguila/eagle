# LIBRERIAS STANDART
from rest_framework.views import APIView
from rest_framework.response import Response

# LIBRERIA PARA EL MANEJO DE FECHAS
from datetime import date,timedelta
from dateutil.relativedelta import relativedelta

# LIBRERIAS PARA ESTADISTICAS Y MANEJO DE DATOS
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# LIBRERIAS PROPIAS -- DATAFRAME

from datos.dataframes import (
                            get_claves,
                            dataframe_key,
                            meses,
                            thounsand_separator,
                            get_dataframe_customers,
                            convert_date,
                            fechas_df,
                            )

# PARA OBTENER LOS DATOS DESDE EL SERVER
from datos.global_functions import get_data_url, requestBasico

from finances.serializers import RecaudacionSerializers


import os
from django.conf import settings
import sys
sys.path.append(settings.ENDPOINT)
import Services

#=================================================#
#================== RECAUDACION ==================#
#=================================================#


class RecaudacionAPIView(APIView):
	"""
	Vista encargada de generar reportes básicos en cuanto a la 
	recaudación generada del endpoint payments en determinadas
	series de tiempo, inicialmente trabaja con el mes actual
	por default, pero recibe parametros para obtener datos en 
	rangos mensuales, trimestrales, semanales y anuales para 
	establecer comparaciones.
	"""

	def get(self, request):

		startDate = fechas_df(self.request.GET.get('startDate','2019-08-01'))
		endDate = fechas_df(self.request.GET.get('endDate','2019-09-07'))
		contexto = dict(
			startDate = startDate,
			endDate = endDate
			)
		try:
			json = self.get_payments(**contexto)
		except Exception as e:
			json = dict(
				results = False,
				errorMsj = 'No se ha podido procesar su petición. Contacte al administrador'
				)
		
		return Response(json)

	def get_last_four_months(self):
		endDate = date.today()
		startDate = endDate - relativedelta(months=4)
		kwargs = dict(
			startDate = startDate,
			endDate = endDate
			)

		url = 'https://190.113.247.198/api/v1/payments/?paid_on__range={startDate},{endDate}'.format(**kwargs)

		pagos = get_data_url(url)

		if np.size(pagos) > 0:
			df = pd.DataFrame(pagos, columns=['customer',
											'paid_on',
											'amount',
											'kind',
											'transfer_id',
											'comment',
											'deposited_on'])\
					.fillna('2001-01-01')


			df['paid_on'] = pd.to_datetime(df.paid_on, 
											format="%Y-%m-%d")\
											.apply(lambda x:x.strftime('%Y-%m-%d'))

			df['annio'] = pd.to_datetime(df.paid_on).dt.year
			df['mes_id'] = pd.to_datetime(df.paid_on).dt.month
			df['mes_calendario'] = df.mes_id.apply(lambda x:meses[str(x)])

			df = df.groupby(['mes_id','mes_calendario']).amount.agg([np.size,np.sum])

			print(df)

			salida = dict(
				results = True,
				)

		else:
			salida = dict(
				results = False,
				errorMsj = 'No hay valores en el rango de fechas seleccionado.'
				)

		# return salida

	def get_payments(self, **kwargs):


		url = 'https://190.113.247.198/api/v1/payments/?paid_on__range={startDate},{endDate}'.format(**kwargs)

		pagos = get_data_url(url)

		if np.size(pagos) > 0:

			df = pd.DataFrame(pagos, columns=['customer',
											'paid_on',
											'amount',
											'kind',
											'transfer_id',
											'comment',
											'deposited_on'])\
					.fillna('2001-01-01')


			df['paid_on'] = pd.to_datetime(df.paid_on, 
											format="%Y-%m-%d")\
											.apply(lambda x:x.strftime('%Y-%m-%d'))

			df['annio'] = pd.to_datetime(df.paid_on).dt.year
			df['mes_id'] = pd.to_datetime(df.paid_on).dt.month
			df['mes_calendario'] = df.mes_id.apply(lambda x:meses[str(x)])
			df['trimestre'] = pd.to_datetime(df.paid_on).dt.quarter
			df['semestre'] = np.where(df['trimestre'].isin([1,2]),1,2)
			df['customer_id'] = df.customer.apply(lambda x:int(x.split('/')[4]))

			df2 = get_dataframe_customers()

			df = df.merge(df2, on=['customer_id'])

			dfA = df.groupby(['kind'])\
					.amount\
					.agg([np.size,
						np.sum])\
					.sort_values(by=['size'],ascending=False)

			dfB = df.groupby(['kind','paid_on','name','rut','commune'])\
					.amount\
					.agg([np.size,
						np.sum])\
					.sort_values(by=['size'],ascending=False)


			dfC = df.groupby(['paid_on'])\
					.amount\
					.agg([np.size,
						np.sum])\
					.sort_values(by=['size'],ascending=False)

			output = list()
			output2 = list()
			filtroB = list()

			for clave in get_claves(dfA):
				output.append({
					'tipo_pago':clave.upper(),
					'nro_servicios':dfA['size'][clave],
					'recaudacion':dfA['sum'][clave],
					})

			for clave in get_claves(dfB):
				output2.append({
					'tipo_pago':clave[0].upper(),
					'fecha_pago':clave[1],
					'id':pd.to_datetime(clave[1]).strftime("%d-%m-%Y"),
					'cliente_name':clave[2],
					'cliente_rut':clave[3],
					'comuna':clave[4],
					'nro_servicios':dfB['size'][clave],
					'recaudacion':dfB['sum'][clave],
					})


			for clave in get_claves(dfC):
				fecha = pd.to_datetime(clave).strftime("%d-%m-%Y")
				filtroB.append({
					'id':fecha,
					'text':fecha + ' - $( ' + thounsand_separator(dfC['sum'][clave]) + ') - (' + \
							thounsand_separator(dfC['size'][clave]) + ' servicios)',
					'nro_servicios':dfC['size'][clave],
					'recaudacion':dfC['sum'][clave],
					'fecha_pago':clave,
					})

			output = sorted(output, key=lambda x:[-x['nro_servicios'],])
			output2 = sorted(output2, key=lambda x:[x['fecha_pago'],x['tipo_pago'],-x['nro_servicios']])
			filtroB = sorted(filtroB, key=lambda x:[x['fecha_pago'],])
			salida = dict(
				results = True,
				nro_servicios =  thounsand_separator(int(np.size(pagos))),
				tableBasic = RecaudacionSerializers(output,many=True).data,
				tableDetail = output2,
				filtroA = self.get_selector(dfA, 'kind'),
				filtroB = filtroB
				)

		else:

			salida = dict(
				results = False,
				errorMsj = 'No hay resultados para el rango de fechas seleccionado.'
				)


		return salida

	def get_selector(self,df,opcion):
		"""
		Metodo encargado de preparar los datos para el selector con un 
		formato especifico solicitado en el front.
		"""
		output = list()

		for clave in get_claves(df):
			output.append(dict(
				id = clave.upper(),
				text = clave.upper() + ' - (' + 
					thounsand_separator(int(df['size'][clave])) + ' servicios)',
				nro_servicios = df['size'][clave],
				recaudacion = df['sum'][clave],
				))

		output = sorted(output,key=lambda x:[-x['nro_servicios']])

		return output




# class RecaudacionAPIView(APIView):
# 	"""
# 	Vista encargada de generar reportes básicos en cuanto a la 
# 	recaudación generada del endpoint payments en determinadas
# 	series de tiempo, inicialmente trabaja con el mes actual
# 	por default, pero recibe parametros para obtener datos en 
# 	rangos mensuales, trimestrales, semanales y anuales para 
# 	establecer comparaciones.
# 	"""

# 	def get(self, request):
# 		tipo = request.GET.get('tipo','semestre')
# 		annio = int(self.request.GET.get('annio',2018))
# 		periodo = int(self.request.GET.get('periodo',1))
# 		if tipo == 'general':
# 			json = self.get_recaudacion_basica()
# 		elif tipo == 'trimestre':
# 			json = self.get_recaudacion_dinamica(tipo,annio,periodo)
# 		elif tipo == 'semestre':
# 			json = self.get_recaudacion_dinamica(tipo,annio,periodo)
# 		elif tipo == 'annio':
# 			json = self.get_recaudacion_dinamica(tipo,annio,periodo)
# 		return Response(json)


# 	def get_recaudacion_dinamica(self,tipo,annio,periodo):

	
# 		try:
# 			url = 'https://190.113.247.198/api/v1/payments/?paid_on__year=%s'% annio
# 			pagos = get_data_url(url)
# 			if np.size(pagos) > 0:
# 				df = pd.DataFrame(pagos).fillna('2001-01-01')

# 				df['paid_on'] = df.paid_on.apply(lambda x:x.split('T')[0])
# 				df['deposited_on'] = df.deposited_on.apply(lambda x:x.split('T')[0])

# 				df['paid_on'] = pd.to_datetime(df.paid_on,format='%Y-%m-%d')
# 				df['deposited_on'] = pd.to_datetime(df.paid_on,format='%Y-%m-%d')

# 				df['annio'] = df.paid_on.dt.year

# 				if tipo == 'trimestre':

# 					df[tipo] = df.paid_on.dt.quarter
# 					df = df[df['trimestre'] == periodo]
					

# 				elif tipo == 'semestre':

# 					df['trimestre'] = df.paid_on.dt.quarter
# 					df[tipo] = np.where(df.trimestre.isin([1,2]),1,2)
# 					df = df[(df['semestre'] == periodo)]

# 				elif tipo == 'annio':
# 					df = df[df['annio'] == annio]

# 				pago = df.groupby(['kind']).amount.agg([np.size,np.sum])
# 				pago = pago.sort_values(by=['size'],ascending=False)


# 				output = list()
# 				for clave in get_claves(pago):
# 					output.append({
# 						'tipo_pago':clave.upper(),
# 						'nro_servicios':pago['size'][clave],
# 						'recaudacion':pago['sum'][clave]
# 						})


# 				output = sorted(output, key=lambda x:[-x['nro_servicios']])

# 				salida = dict(
# 					results = 'Ok',
# 					tableBasic = output, #ESTE DATO SIRVE PARA EL GRAFICO
# 					)

# 				return salida
# 			else:
# 				return {
# 					'results' : 'Error',
# 					'textMensaje':'No hay resultados para mostrar',
# 					'tableBasic' : list()
# 					}


# 		except Exception as e:
# 			return {'results' : 'Error',
# 					'error':'No hay respuesta'}



# 	def get_recaudacion_basica(self):
# 		"""
# 		Se obtiene la recaudación por default
# 		"""

# 		startDate = self.request.GET.get('startDate',None)
# 		endDate = self.request.GET.get('endDate',None)

# 		datos = False #Bandera


# 		if startDate and endDate:
# 			datos = True
# 		else:
# 			datos = False

# 		if not datos:

# 			hoy = date.today()

# 			startDate = date(hoy.year,hoy.month,1).strftime('%Y-%m-%d')
# 			endDate = hoy.strftime('%Y-%m-%d')

# 			rangos = dict(
# 				startDate = startDate,
# 				endDate = endDate
# 			)
# 		else:

# 			rangos = dict(
# 				startDate = pd.to_datetime(startDate,format="%Y-%m-%d"),
# 				endDate = pd.to_datetime(endDate,format="%Y-%m-%d")
# 			)


# 		url = 'https://190.113.247.198/api/v1/payments/?paid_on__range={startDate},{endDate}'.format(**rangos)
# 		pagos = get_data_url(url)

# 		df = pd.DataFrame(pagos)

# 		pago = df.groupby(['kind']).amount.agg([np.size,np.sum])
# 		pago = pago.sort_values(by=['size'],ascending=False)

# 		output = list()
# 		for clave in get_claves(pago):
# 			output.append({
# 				'tipo_pago':clave.upper(),
# 				'nro_servicios':pago['size'][clave],
# 				'recaudacion':pago['sum'][clave]
# 				})


# 		output = sorted(output, key=lambda x:[-x['nro_servicios']])

# 		salida = dict(
# 			tableBasic = output, #ESTE DATO SIRVE PARA EL GRAFICO
# 			)

# 		return salida


#=================================================#
#=============== FACTURA VS PAGOS ================#
#=================================================#

class FacturacionVsRecaudacionAPIView(APIView):


	def get(self, request):

		endpoint = self.request.GET.get('endpoint','payments')
		startDate = self.request.GET.get('startDate','2019-01-01')
		endDate = self.request.GET.get('endDate','2019-09-01')
		formato = "%Y-%m-%d"
		contexto = dict(
			endpoint = endpoint,
			startDate = pd.to_datetime(startDate,format=formato).strftime(formato),
			endDate = pd.to_datetime(endDate,format=formato).strftime(formato),
			)

		try:
			if endpoint == 'payments' or endpoint == 'invoices':
				json = self.get_comparaciones(**contexto)

			else:
				json = None


			return Response(json)
		except Exception as e:
			salida = dict(
				results = False,
				errorMsj = 'Upps, algo ha salido mal, por favor ponganse en contacto con el administrador del sistema.'
				)
		
			return Response(salida)

	def dataframe_servicio(self):
		df = pd.DataFrame(Services.services, 
				columns=['customer_name','customer_rut',
						'number','composite_address',
						'customer_id']).fillna('2001-01-01')
		return df

	def get_comparaciones(self, **kwargs):
		formato = "%Y-%m-%d"
		df2 = self.dataframe_servicio()

		startDate = kwargs['startDate']
		endDate = kwargs['endDate']

		if kwargs['endpoint'] == 'payments':
			url = 'https://190.113.247.198/api/v1/payments/?paid_on__range={startDate},{endDate}'.format(**kwargs)
			profit = 'amount'
			columnas = ['paid_on','deposited_on','amount','kind','customer']
		elif kwargs['endpoint'] == 'invoices':
			url = 'https://190.113.247.198/api/v1/invoices/?paid_on__range={startDate},{endDate}'.format(**kwargs)
			profit = 'total'
			columnas = ['paid_on','deposited_on','total','kind','customer']

		pagos = get_data_url(url)

		if np.size(pagos) > 0:
			try:

				df = pd.DataFrame(pagos, 
						columns = columnas)\
					.fillna('2001-01-01')

				df['customer_id'] = df.customer.apply(lambda x:int(x.split('/')[4]))
				df['paid_on'] = df.paid_on.apply(lambda x:x.split('T')[0])
				# df['deposited_on'] = df.deposited_on.apply(lambda x:x.split('T')[0])									
				
				df['paid_on'] = pd.to_datetime(df.paid_on, format = formato)
				# df['deposited_on'] = pd.to_datetime(df.deposited_on, format = formato)

				df['annio'] = df.paid_on.dt.year
				df['mes_id'] = df.paid_on.dt.month
				df['mes_calendario'] = df['mes_id'].apply(lambda x:meses[str(x)])
				df['trimestre'] = df.paid_on.dt.quarter
				df['semestre'] = np.where(df['trimestre'].isin([1,2]),1,2)

				df = df[(df['paid_on'] != '2001-01-01')]

				df = df.merge(df2, on=['customer_id'])


				dfA = df.groupby(['kind'])[profit]\
						.agg([np.size,np.sum,np.mean])\
						.sort_values(by=['size'],ascending=False)\
						.rename(columns={'size':'nro_servicios',
										'sum':'recaudacion',
										'mean':'promedio'})

				dfAA = df.groupby(['kind','customer_name',
									'customer_rut','composite_address',
									'paid_on'])[profit]\
						.agg([np.size,np.sum])\
						.sort_values(by=['sum'],ascending=False)\
						.rename(columns={'size':'nro_servicios',
										'sum':'recaudacion',
										})

				dfAAA = df.groupby(['paid_on'])[profit]\
						.agg([np.size,np.sum])\
						.sort_values(by=['sum'],ascending=False)\
						.rename(columns={'size':'nro_servicios',
										'sum':'recaudacion',
										})


				output,output2 = list(),list()
				for clave in get_claves(dfA):
					output.append({
						'tipo_pago':clave.upper(),
						'nro_servicios':dfA['nro_servicios'][clave],
						'recaudacion':dfA['recaudacion'][clave],
						'promedio':dfA['promedio'][clave],

						})

				diccionario = dict(
					invoices = 'facturas',
					payments = 'pagos'
					)

				filtroA = list(map(lambda x:{'id':x['tipo_pago'].upper(),
											'text':x['tipo_pago'].upper() + \
											' $('+thounsand_separator(int(x['recaudacion'])) + 
											') - (' + thounsand_separator(int(x['nro_servicios'])) + 
											' '+diccionario[kwargs['endpoint']]+')',
											'nro_servicios':x['nro_servicios'],
											'recaudacion':x['recaudacion'],
											},output))

				for clave in get_claves(dfAA):
					output2.append({
						'tipo_pago':clave[0].upper(),
						'cliente_name':clave[1],
						'cliente_rut':clave[2],
						'direccion':clave[3],
						'fecha_pago':pd.to_datetime(clave[4]).strftime('%d-%m-%Y'),
						'nro_servicios':dfAA['nro_servicios'][clave],
						'recaudacion':dfAA['recaudacion'][clave],
						'endpoint':diccionario[kwargs['endpoint']],

						})

				filtroB = list()

				for clave in get_claves(dfAAA):
					filtroB.append({
						'id':pd.to_datetime(clave).strftime("%d-%m-%Y"),
						'text':pd.to_datetime(clave).strftime("%d-%m-%Y") + ' $(' + 
							thounsand_separator(int(dfAAA['recaudacion'][clave]))\
							 + ') - (' + thounsand_separator(int(dfAAA['nro_servicios'][clave])) 
							 + ' '+diccionario[kwargs['endpoint']]+')',
						'nro_servicios':dfAAA['nro_servicios'][clave],
						'recaudacion':dfAAA['recaudacion'][clave],
						'fecha_pago':clave

						})


				output = sorted(output, key=lambda x:[-x['nro_servicios']])
				output2 = sorted(output2, key=lambda x:[-x['recaudacion']])
				filtroA = sorted(filtroA,key=lambda x:[-x['nro_servicios']])
				filtroB = sorted(filtroB,key=lambda x:[x['fecha_pago']])

				salida = dict(
					results = True,
					tableBasic = output,
					tableDetail = output2,
					filtroA = filtroA,
					filtroB = filtroB,
					nro_servicios = np.size(pagos)
					)
			except Exception as e:
				salida = dict(
					results = False,
					errorMsj = 'Ha ocurrido un error durante el procesamiento de datos.'
					)

		else:
			salida = dict(
				results = False,
				errorMsj = 'No hay registros para el rango de fechas seleccionado.'
				)

		return salida


#=================================================#
#================== FACTURACIÓN ==================#
#=================================================#


class FacturaAPIView(APIView):
	"""
	API ENCARGADA DE MOSTRAR LOS DETALLES DE LOS PAGOS AGRUPADOS POR
	EL TIPO DE DOCUMENTO ADEMÁS DE BRINDAR ESTADISTICAS
	"""

	def get(self, request):

		startDate = convert_date(self.request.GET.get('startDate','2019-04-01'))
		endDate = convert_date(self.request.GET.get('endDate','2019-06-30'))
		contexto = dict(
			startDate = startDate,
			endDate = endDate
			)

		return Response(self.get_facturas_by_doc(**contexto))

	def get_facturas_by_doc(self,**kwargs):
		"""
		METODO QUE PREPARA LOS DATOS OBTENIDOS EN EL 
		ENDPOINT FACTURAS CLASIFICADOS POR EL TIPO DE DOCUMENTO
		EN UNA SECCIÓN DEL TIEMPO DETERMINADO
		"""

		url = 'https://190.113.247.198/api/v1/invoices/?paid_on__range={startDate},{endDate}'.format(**kwargs)
		try:
			facturas = get_data_url(url)
			if np.size(facturas) > 0:

				df = pd.DataFrame(facturas).fillna('2001-01-01') #OBJETO DATAFRAME DE PANDAS

				#===== FECHAS =====#
				df['paid_on'] = pd.to_datetime(df.paid_on,format="%Y-%m-%d")\
									.apply(lambda x:x.strftime("%Y-%m-%d"))
				df['annio'] = pd.to_datetime(df.paid_on, format="%Y-%m-%d").dt.year
				df['mes_id'] = pd.to_datetime(df.paid_on, format="%Y-%m-%d").dt.month
				df['mes'] = df.mes_id.apply(lambda x:meses[str(x)])

				df['trimestre'] = pd.to_datetime(df.paid_on, format="%Y-%m-%d").dt.quarter
				df['semestre'] = np.where(df['trimestre'].isin([1,2]),1,2)

				df['customer_id'] = df.customer.apply(lambda x:int(x.split('/')[4]))

				
				#===== DATAFRAME DE CLIENTES PARA EXTRAER DATOS RELEVANTES =====#
				df2 = get_dataframe_customers()

				#===== MERGE DE LOS 2 DATAFRAMES =====#
				df = df.merge(df2, on=['customer_id'])

				#===== FILTROS =====#
				df = df[df['paid_on'] != '2001-01-01'] # EXCLUYO RESULTADOS NULOS

				dfA = df.groupby(['kind'])\
						.total\
						.agg([np.size,np.sum])\
						.sort_values(by=['size','sum'],ascending=False)\
						.rename(columns={'size':'nro_servicios',
										'sum':'recaudacion'})

				filtroA = list()


				dfB = df.groupby(['kind','annio',
								'mes_id','mes',
								'comment','folio',
								'paid_on','due_date','name',
								'rut'])\
						.total\
						.agg([np.size,np.sum])\
						.sort_values(by=['paid_on','mes_id','sum'],ascending=True)\
						.rename(columns={'size':'nro_servicios',
										'sum':'recaudacion'})


				output = list()
				for clave in get_claves(dfA):
					output.append({
						'tipo_pago':clave,
						'nro_servicios':dfA['nro_servicios'][clave],
						'recaudacion':dfA['recaudacion'][clave]
						})

					filtroA.append({
						'id':clave,
						'text':clave + ' - $(' + thounsand_separator(dfA['recaudacion'][clave]) + \
							') - (' + thounsand_separator(dfA['nro_servicios'][clave]) + ' servicios)',
						'nro_servicios':dfA['nro_servicios'][clave],
						'recaudacion':dfA['recaudacion'][clave]
						})


				output2 = list()
				for clave in get_claves(dfB):
					fecha_pago = pd.to_datetime(clave[6]).strftime("%d-%m-%Y")
					fecha_vencimiento = pd.to_datetime(clave[7]).strftime("%d-%m-%Y")
					output2.append({
						'tipo_pago':clave[0],
						'annio':clave[1],
						'mes_id':clave[2],
						'mes':clave[3],
						'comentario':clave[4],
						'folio':clave[5],
						'fecha_pago':fecha_pago,
						'fecha_vencimiento':fecha_vencimiento,
						'cliente_name':clave[8],
						'cliente_rut':clave[9],
						'nro_servicios':dfB['nro_servicios'][clave],
						'recaudacion':dfB['recaudacion'][clave]
						})

				output = sorted(output,key=lambda x:[-x['nro_servicios']])
				output2 = sorted(output2,key=lambda x:[x['mes_id'],-x['recaudacion']])
				filtrosBC = self.filtros(df)
				salida = dict(
					results = True,
					tableBasic = output,
					tableDetail = output2,
					filtroA = sorted(filtroA, key=lambda x:-x['nro_servicios']),
					filtroB = filtrosBC['filtroB'],
					filtroC = filtrosBC['filtroC'],
					)

				return salida


			else:

				return {
					'results':False,
					'errorMsj':'No hay resultados para mostrar en este rango de fechas.'
					}
		except Exception as e:
			salida = dict(
				results = False,
				errorMsj = 'Upps, lo sentimos ha ocurrido un error con la fuente de datos, contacta al administrador.'
				)
			return salida
	
	def filtros(self, df):
		"""
		METODO CONSTRUCTOR DE LOS FILTROS PARA LAS TABLAS
		"""
		filtroB = list()
		filtroC = list()

		#===== FILTRO POR MES =====#

		dfA = df.groupby(['mes','mes_id'])\
				.total\
				.agg([np.size,np.sum])\
				.sort_values(by=['size'],ascending=False)\
				.rename(columns={'size':'nro_servicios',
								'sum':'recaudacion'})

		for clave in get_claves(dfA):
			filtroB.append({
				'id':clave[0],
				'text':clave[0] + ' - $(' + thounsand_separator(dfA['recaudacion'][clave])\
						 + ') - (' + thounsand_separator(dfA['nro_servicios'][clave]) + ' servicios)',
				'nro_servicios':dfA['nro_servicios'][clave],
				'recaudacion':dfA['recaudacion'][clave],
				'mes_id':clave[1]
				})

		#===== FILTRO POR FECHA =====#
		# ME PERMITE DETERMINAR MI GANANCIA Y VENTAS POR DÍA #

		dfB = df.groupby(['paid_on'])\
				.total\
				.agg([np.size,np.sum])\
				.sort_values(by=['size'],ascending=False)\
				.rename(columns={'size':'nro_servicios',
								'sum':'recaudacion'})

		for clave in get_claves(dfB):
			fecha_pago = pd.to_datetime(clave).strftime("%d-%m-%Y")
			filtroC.append({
				'id':fecha_pago,
				'text':fecha_pago + ' - $(' + thounsand_separator(dfB['recaudacion'][clave])\
						 + ') - (' + thounsand_separator(dfB['nro_servicios'][clave]) + ' servicios)',
				'nro_servicios':dfB['nro_servicios'][clave],
				'recaudacion':dfB['recaudacion'][clave],
				})

		filtroB = sorted(filtroB,key=lambda x:[x['mes_id'],-x['nro_servicios']])
		filtroC = sorted(filtroC,key=lambda x:[x['id']])

		salida = dict(
			filtroB = filtroB,
			filtroC = filtroC
			)

		return salida

#=================================================#
#============ OS TERMINADAS ICLASS ===============#
#=================================================#

class IClassAPIView(APIView):
	"""
	API ENCARGADA DE MOSTRAR LAS ORDENES DE SERVICIO
	TERMINADAS PROVENIENTES DE ICLASS
	"""

	def get(self, request):
		return Response(self.get_os())

	def get_os(self, **kwargs):

		hoy = date.today()
		mes_en_curso = 6 #hoy.month

		print(mes_en_curso)

		path = settings.CSV + '/OSterminadasM.csv'

		df = pd.read_csv(path,
						usecols=['os','ciudad',
								'barrio','motivo',
								'programación','criado por'])\
				.fillna('2000-01-01')

		print("====================================================================================")

		df['programación'] = pd.to_datetime(df['programación'])
		df['mes'] = df['programación'].dt.month
		df['programación'] = df['programación'].apply(lambda x:x.strftime("%d-%m-%Y"))
		df['ciudad'] = df.ciudad.apply(lambda x:x.lower().replace(' ',''))
		df['barrio'] = df.barrio.apply(lambda x:x.lower()\
												.replace(' ','')\
												.replace('estacióncentral','estación central')\
												.replace('quintanormal','quinta normal')\
												.replace('sanmartin841','san martin 841')\
												.replace('sanjoaquín','san joaquín')\
												.replace('sanmiguel','san miguel')\
												.replace('elparron0681','el parron 0681')\
												.replace('locruzaz0525','locruzaz 0525')\
												.replace('elparroncondominiodonvictorpiso5','el parron condominio don victor piso 5')
												)


		df = df[(df['programación'] != '2001-01-01')&(df['mes'] == mes_en_curso)]

		dfA = df.groupby(['ciudad','barrio'])\
				.os\
				.agg([np.size])\
				.sort_values(by=['size'],ascending=False)\
				.rename(columns={'size':'nro_servicios'})

		dfB = df.groupby(['programación','mes'])\
				.os\
				.agg([np.size])\
				.sort_values(by=['programación'],ascending=True)\
				.rename(columns={'size':'nro_servicios'})

		dfBB = df.groupby(['programación','mes'])\
				.os\
				.agg([np.size])\
				.sort_values(by=['programación'],ascending=True)\
				.rename(columns={'size':'nro_servicios'}).diff()

		dfB = dfB.merge(dfBB, on=['programación'])

		print(dfA)
		print("\n====================================================================================\n")
		print(dfB)
		return df['barrio']