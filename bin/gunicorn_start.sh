#!/bin/bash

NAME="190.113.247.216" # Name of the application
DJANGODIR=/opt/envEagle/eagle # Django project directory
LOGFILE=/var/log/gunicorn/gunicorn_aguila.log
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/opt/envEagle/eagle/run/gunicorn.sock # we will communicate using this unix socket
USER=develaguila # the user to run as
GROUP=develaguila # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=aguila.settings.local # which settings file should Django use
DJANGO_WSGI_MODULE=aguila.wsgi
TIMEOUT=600

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source /opt/envEagle/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--timeout $TIMEOUT \
--log-file=$LOGFILE 2>>$LOGFILE
