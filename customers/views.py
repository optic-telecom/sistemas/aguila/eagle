# LIBRERIAS STANDART
from rest_framework.views import APIView
from rest_framework.response import Response

# LIBRERIA PARA EL MANEJO DE FECHAS
from datetime import date,timedelta
from dateutil.relativedelta import relativedelta

# LIBRERIAS PARA ESTADISTICAS Y MANEJO DE DATOS
from scipy import stats
import numpy as np
import pandas as pd


import os
from django.conf import settings

# LIBRERIAS PROPIAS -- DATAFRAME

from datos.dataframes import (get_dataframe_services,
                            get_claves,
                            dataframe_key,
                            thounsand_separator,
                            fechas_df,
                            )

from customers.serializers import ContractsCustomerSerializers


import sys
sys.path.append(settings.ENDPOINT)
import Services, Nodes, Sellers, Plans, Customers


#=====================================================================#
#========================= SELECTOR API ==============================#
#=====================================================================#


class DinamicAPIView(APIView):
	"""
	API SE ENCARGA DE SERVIR EL JSON PARA LOS SELECTORES 
	MULTIPLES DE LA VISTA DINAMICA PARA LOS SERVICIOS, 
	ANTIGUAMENTE LLAMADOS CONTRATOS.
	"""

	filtroPpal = dict(
		service_name = ['service_name','service_price'],
		get_status_display = ['get_status_display'],
		commune = ['commune'],
		node_code = ['node_code'],
		document_type = ['document_type'],
		seller_name = ['seller_name']
		)

	def get(self, request):

		startDate = self.request.GET.get('startDate','2019-01-01')
		endDate = self.request.GET.get('endDate','2019-03-01')
		peticion = int(self.request.GET.get('peticion','4'))

		opcionA = self.request.GET.get('opcionA','service_name').lower()
		eleccionA = eval(self.request.GET.get('eleccionA',
						"['2018 - Fibra - Internet 150Megas',\
						'2018 - Fibra - Internet 30Megas']"))
		opcionB = self.request.GET.get('opcionB','commune').lower()
		eleccionB = eval(self.request.GET.get('eleccionB',
						"['Estación Central',\
						'Santiago']"))
		opcionC = self.request.GET.get('opcionC','get_status_display').lower()
		eleccionC = eval(self.request.GET.get('eleccionC',"['activo','moroso']"))
		contexto = dict(
			startDate = startDate,
			endDate = endDate,
			peticion = peticion,
			opcionA = opcionA,
			eleccionA = eleccionA,
			opcionB = opcionB,
			eleccionB = eleccionB,
			opcionC = opcionC
		)

		if peticion == 0:
			contexto = dict(
				startDate = startDate,
				endDate = endDate,
				peticion = peticion
			)
			json = self.peticion_zero(**contexto)
		elif peticion in [1,2,3]:
			json = self.peticion_select2(**contexto)
		elif peticion == 4:
			
			contexto = dict(
				startDate = startDate,
				endDate = endDate,
				peticion = peticion,
				opcionA = opcionA,
				eleccionA = eleccionA,
				opcionB = opcionB,
				eleccionB = eleccionB,
				opcionC = opcionC,
				eleccionC = eleccionC
			)
			json = self.peticion_four(**contexto)
		else:
			json = dict(
				results = False,
				errorMsj = 'Usted ha seleccionado una opción invalida para realizar cualquier acción.'
				)


		return Response(json)


	def get_my_dataframe(self, **kwargs):
		"""
		Prepara el dataframe de manera común para todos los metodos,
		aplicandole el filtro por fecha lo cual es algo comun.
		"""

		df = get_dataframe_services()

		mask = (df['activated_on']>=kwargs['startDate'])&(df['activated_on']<=kwargs['endDate'])
		df = df[mask]

		return df

	def get_selector(self,df,opcion):
		"""
		Metodo encargado de preparar los datos para el selector con un 
		formato especifico solicitado en el front.
		"""
		output = list()
		if opcion == 'service_name':

			for clave in get_claves(df):
				output.append(dict(
					id = clave[0],
					text = clave[0].upper() + ' - $(' + 
						thounsand_separator(int(clave[1])) + ') - (' + 
						thounsand_separator(int(df['size'][clave])) + ' servicios)',
					nro_servicios = df['size'][clave],
					recaudacion = df['sum'][clave],
					))

		else:

			for clave in get_claves(df):
				output.append(dict(
					id = clave,
					text = clave.upper() + ' - (' + 
						thounsand_separator(int(df['size'][clave])) + ' servicios)',
					nro_servicios = df['size'][clave],
					recaudacion = df['sum'][clave],
					))

		output = sorted(output,key=lambda x:[-x['nro_servicios']])

		return output

	def peticion_zero(self, **kwargs):
		"""
		Método encargado de servir la cantidad de servicios y la recaudación general 
		por un rango de fechas seleccionado. 
		"""

		df = self.get_my_dataframe(**kwargs)

		if np.size(df['id']) > 0:

			salida = dict(
				results = True,
				nro_servicios = np.size(df['id']),
				nro_elementos = thounsand_separator(int(np.size(df['id']))) + ' servicios',
				recaudacion = '$(' + thounsand_separator(int(np.sum(df['service_price']))) + ')' 

				)
		else:
			salida = dict(
				results = False,
				errorMsj = 'No hay resultados para el rango de fechas seleccionado.'
				)

		return salida

	def peticion_select2(self, **kwargs):
		"""
		Este metodo envia al front los elementos necesarios
		para cargarlos en cada selector multiple con las 
		etiquetas necesarias
		"""
		df = self.get_my_dataframe(**kwargs)

		peticion = kwargs['peticion']

		try:
			if peticion == 1:
				opcionA = kwargs['opcionA']
				x = self.filtroPpal[opcionA]

			elif peticion == 2:

				opcionB = kwargs['opcionB']

				opcionA = kwargs['opcionA']
				eleccionA = kwargs['eleccionA']

				x = self.filtroPpal[opcionB]

				mask = (df[opcionA].isin(eleccionA))
				df = df[mask]

			elif peticion == 3:
				opcionC = kwargs['opcionC']

				opcionA = kwargs['opcionA']
				eleccionA = kwargs['eleccionA']
				opcionB = kwargs['opcionB']
				eleccionB = kwargs['eleccionB']

				x = self.filtroPpal[opcionC]


				mask = (df[opcionA].isin(eleccionA))&(df[opcionB].isin(eleccionB))
				df = df[mask]


			df = df.groupby(x)\
					.service_price\
					.agg([np.size,np.sum])\
					.sort_values(by=['size'], ascending=False)

			if peticion == 1:
				salida = dict(
					results = True,
					eleccionA = self.get_selector(df,opcionA)
					)
			elif peticion == 2:
				salida = dict(
					results = True,
					eleccionB = self.get_selector(df,opcionB)
					)
			elif peticion == 3:
				salida = dict(
					results = True,
					eleccionC = self.get_selector(df,opcionC)
					)

			return salida
		except Exception as e:
			salida = dict(
				results = False,
				errorMsj = 'Ha ocurrido un error al procesar sus datos.'
				)

	def peticion_four(self,**kwargs):
		"""	
		Metodo encargado de enviar al front
		el datatable principal y el detalle,
		el grafico sankey, el de linea de tiempo,
		los filtros A,B y C
		"""
		output = list()
		output2 = list()
		

		sankey = list()
		fechas = list()

		df = self.get_my_dataframe(**kwargs)

		opcionA = kwargs['opcionA']
		eleccionA = kwargs['eleccionA']
		opcionB = kwargs['opcionB']
		eleccionB = kwargs['eleccionB']
		opcionC = kwargs['opcionC']
		eleccionC = kwargs['eleccionC']

		try:

			mask = (df[opcionA].isin(eleccionA))\
					&(df[opcionB].isin(eleccionB))\
					&(df[opcionC].isin(eleccionC))

			df = df[mask]

			dfB = df.groupby([opcionA,opcionB,opcionC])\
					.service_price\
					.agg([np.size,np.sum,
						np.mean,np.median,
						min,max])\
					.sort_values(by=['size'],ascending=False)

			for clave in get_claves(dfB):
				output.append({
					'opcionA':clave[0],
					'opcionB':clave[1],
					'opcionC':clave[2],
					'nro_servicios':dfB['size'][clave],
					'recaudacion':dfB['sum'][clave],
					'promedio':dfB['mean'][clave],
					'mediana':dfB['median'][clave],
					'minimo':dfB['min'][clave],
					'maximo':dfB['max'][clave],
					})

				sankey.append({
					'from':clave[0],
					'to':clave[1],
					'value':dfB['sum'][clave],
					'value2':dfB['size'][clave]
					})

				sankey.append({
					'from':clave[1],
					'to':clave[2],
					'value':dfB['sum'][clave],
					'value2':dfB['size'][clave]
					})

			dfD = df.groupby([opcionA,opcionB,opcionC,
							'customer_name','customer_rut',
							'number','composite_address','activated_on'])\
					.service_price\
					.agg([np.size,np.sum,np.mean
						])\
					.sort_values(by=['size'],ascending=False)

			for clave in get_claves(dfD):
				output2.append({
					'opcionA':clave[0],
					'opcionB':clave[1],
					'opcionC':clave[2],
					'cliente_name':clave[3],
					'cliente_rut':clave[4],
					'nro_contrato':clave[5],
					'direccion':clave[6],
					'fecha_activacion':pd.to_datetime(clave[7]).strftime('%d-%m-%Y'),
					'nro_servicios':dfD['size'][clave],
					'recaudacion':dfD['sum'][clave],
					'promedio':dfD['mean'][clave],

					})

			dfF = df.groupby(['activated_on'])\
					.service_price\
					.agg([np.size,np.sum])\
					.sort_values(by=['activated_on'],ascending=True)

			for clave in get_claves(dfF):
				f_a = pd.to_datetime(clave).strftime('%d-%m-%Y')
				fechas.append({
					'id':f_a,
					'text': f_a + ' - (' + 
						thounsand_separator(int(dfF['size'][clave])) + ' servicios)',
					'nro_servicios':dfF['size'][clave],
					'recaudacion':dfF['sum'][clave],
					'fecha_activacion':clave

					})

			output = sorted(output,key=lambda x:[-x['nro_servicios']])
			output2 = sorted(output2,key=lambda x:[-x['recaudacion']])
			fechas = sorted(fechas,key=lambda x:[x['fecha_activacion']])
			sankey = sorted(sankey,key=lambda x:[-x['value2']])

			filtroA = self.get_filtro(opcionA,**kwargs)
			filtroB = self.get_filtro(opcionB,**kwargs)
			filtroC = self.get_filtro(opcionC,**kwargs)

			salida = dict(
				results = True,
				tableBasic = output,
				tableDetail = ContractsCustomerSerializers(output2,many=True).data,
				fechas = fechas,
				sankey = sankey,
				filtroA = filtroA,
				filtroB = filtroB,
				filtroC = filtroC,
				)
		except Exception as e:
			salida = dict(
				results = False,
				errorMsj = 'Upps, ha ocurrido un error en el procesamiento de sus datos, contacte al administrador del sistema.'

				)


		return salida

	def get_filtro(self,opcion,**kwargs):

		df = self.get_my_dataframe(**kwargs)

		opcionA = kwargs['opcionA']
		eleccionA = kwargs['eleccionA']
		opcionB = kwargs['opcionB']
		eleccionB = kwargs['eleccionB']
		opcionC = kwargs['opcionC']
		eleccionC = kwargs['eleccionC']

		mask = (df[opcionA].isin(eleccionA))\
				&(df[opcionB].isin(eleccionB))\
				&(df[opcionC].isin(eleccionC))

		df = df[mask]

		x = self.filtroPpal[opcion]

		df = df.groupby(x)\
				.service_price\
				.agg([np.size,np.sum])\
				.sort_values(by=['size'], ascending=False)

		return self.get_selector(df,opcion)