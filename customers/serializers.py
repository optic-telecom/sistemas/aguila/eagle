from rest_framework import serializers

class AbstractNumsSerializers(serializers.Serializer):
	nro_servicios = serializers.IntegerField()
	#nro_servicios_p = serializers.FloatField()
	recaudacion = serializers.FloatField()
	#recaudacion_p = serializers.FloatField()
	promedio = serializers.FloatField()


class ContractsCustomerSerializers(AbstractNumsSerializers):
	opcionA = serializers.CharField(max_length=255)
	opcionB = serializers.CharField(max_length=255)
	opcionC = serializers.CharField(max_length=255)
	cliente_name = serializers.CharField(max_length=255)
	cliente_rut = serializers.CharField(max_length=255)
	nro_contrato = serializers.CharField(max_length=255)
	direccion = serializers.CharField(max_length=255)
	fecha_activacion = serializers.CharField(max_length=50)
	botones = serializers.SerializerMethodField()

	def get_botones(self, obj):

		botones = '<center>'
		botones += '<a title="Contrato matrix nro. '+str(obj['nro_contrato'])+'" href="https://optic.matrix2.cl/contracts/'+str(obj['nro_contrato'])+'/" target="_blank" class="btn btn-xs btn-info text-inverse"><i class="fa fa-link"></i></a>&nbsp;'
		botones += '</center>'
		return botones
