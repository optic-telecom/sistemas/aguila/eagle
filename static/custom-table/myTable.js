var myTable = function(){

	columns = []
	palabraClave = 'registros'
	order_by_columns = []
	div = "#example"
	displayLength = 0
	servicio_col = 1
	recaudacion_col = 3

	this.get_columns = function(){

		output = []
		for(i=0;i<this.columns.length;i++){
			if((this.columns[i]==='botones') || 
			(this.columns[i] == 'nro_servicios') || 
			(this.columns[i] == 'recaudacion') || 
			(this.columns[i] == 'annio')){
				output.push({
				'data':this.columns[i],
				'className':'text-center'
				})
			}else{
				output.push({
				'data':this.columns[i],
				'className':'text-nowrap'
				})
			}
		}
		return output
	}

	this.language = function(){
        language = {
            "decimal": ",",
            "thousands": ".",
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ " + this.palabraClave,
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando " + this.palabraClave + " del _START_ al _END_ de un total de _TOTAL_ " + this.palabraClave,
            "sInfoEmpty":      "Mostrando " + this.palabraClave + " del 0 al 0 de un total de 0 " + this.palabraClave,
            "sInfoFiltered":   "(filtrado de un total de _MAX_ " + this.palabraClave + ")",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            'buttons': {
            'copy': "Copiar",
            'print':'Imprimir',
            'colvis':'Columnas Visibles',
            },
        }

	    return language

	}

	this.columnDefs = function(){
		
		no_order = (this.columns.length - 1)
		output = []
		output.push({'targets':no_order, 'orderable':false, 'searchable':false})		
		return output

	}

	this.render_table = function(data){

		var obj = {
			'data':data,
			'columns':this.get_columns(),
			'language':this.language(),
			'destroy':true,
			'order':[this.order_by_columns],
			'columnDefs':this.columnDefs(),
			'autoWidth': true,
			'responsive':true,
			"paging": true,
			"processing": true,
			"pagingType": "full_numbers"

		}
		if(this.displayLength!=0){
			obj['displayLength'] = this.displayLength
			obj['aLengthMenu'] = [[this.displayLength,10, 25, 50, 100,-1], [this.displayLength,10, 25, 50, 100,"todos"]]
		}

       return obj;

	}





}


