
userInSession();
App.setPageTitle('Águila | Clientes');
titlePage('Clientes - General')

var json = {}

$(document).ready(function(){

	$.when(
	
		dateRange("#default-daterange"),
		$(".select2").select2(json_select2),
	
	)
	.then(function(){
			$("#container_ppal").removeClass('filtros')
	})

})


function dateRange(id){
    var start = moment().subtract(29, 'days');
    var end = moment();

    var quarter = moment().quarter();
    // Aquí se inicializa el selector de fechas
    $(id + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    //===== TRIMESTRES =====\\
    /******** FORMATO AAAA-MM-DD ********/
    var Q1A = moment().year().toString() + '-01-01'
    var Q1B = moment().year().toString() + '-03-31'

    var Q2A = moment().year().toString() + '-04-01'
    var Q2B = moment().year().toString() + '-06-30'

    var Q3A = moment().year().toString() + '-07-01'
    var Q3B = moment().year().toString() + '-09-30'

    var Q4A = moment().year().toString() + '-10-01'
    var Q4B = moment().year().toString() + '-12-31'

    //===== SEMESTRE =====\\

    var S1A = moment().year().toString() + '-01-01'
    var S1B = moment().year().toString() + '-06-30'

    var S2A = moment().year().toString() + '-07-01'
    var S2B = moment().year().toString() + '-12-31'

    $(id).daterangepicker({
        startDate: start,
        endDate: end,
        showDropdowns: true,
        minDate: '01/01/2001',
        maxDate: '01/01/2099',
        opens: 'right',
		drops: 'down',
		buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-warning',
        cancelClass: 'btn-inverse',
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Enviar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Seleccionar Fecha',
            todayLabel: 'Hoy',
            daysOfWeek: [
            "Lun",
            "Mar",
            "Mié",
            "Jue",
            "Vie",
            "Sáb",
            "Dom"],
            monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
            ],
            firstDay: 0
        },
        ranges: {
        'Hoy': [moment(), moment()],
        'Mes actual': [moment().startOf('month'), moment().endOf('month')],
        'Últimos 15 días': [moment().subtract(14, 'days'), moment()],
        'Últimos 30 días': [moment().subtract(29, 'days'), moment()],

        'Trimestre 1': [moment(Q1A,'YYYY-MM-DD'), moment(Q1B,'YYYY-MM-DD')],
        'Trimestre 2': [moment(Q2A,'YYYY-MM-DD'), moment(Q2B,'YYYY-MM-DD')],
        'Trimestre 3': [moment(Q3A,'YYYY-MM-DD'), moment(Q3B,'YYYY-MM-DD')],
        'Trimestre 4': [moment(Q4A,'YYYY-MM-DD'), moment(Q4B,'YYYY-MM-DD')],

        'Semestre 1': [moment(S1A,'YYYY-MM-DD'), moment(S1B,'YYYY-MM-DD')],
        'Semestre 2': [moment(S2A,'YYYY-MM-DD'), moment(S2B,'YYYY-MM-DD')],

        'Mes actual': [moment().startOf('month'), moment().endOf('month')],
        'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Año anterior': [moment().subtract(364, 'days'), moment()],//
        }
        });

    //===== PARAMETROS PARA PETICIONES AJAX O AXIOS =====\\

    $(id).on('apply.daterangepicker', function(ev, picker) {
        /*
            MANEJADOR DE EVENTOS PROPIOS DEL DETERANGE, INICIA 
            CUANDO PULSAMOS EL BTN ACEPTAR(APPLY) DEL CALENDARIO
            DESPLEGADO.
        */

        ev.preventDefault()

        $("#opcionA,#opcionB,#opcionC").empty().html('<option value="todos">..:: Opción ::..</option>');
        $("#eleccionA,#eleccionB,#eleccionC,#chartdiv2,#chartdiv").empty();

        $("#card2,#tab2,#tab3").addClass('filtros');
        $("#tab_grafico").trigger('click');

            /* vaciamos el resumen del span*/
        $("#primer_resumen,#resumen")
        .html('<h5 class="text-center text-white"></h5>');

        $("#startDate").val(picker.startDate.format('YYYY-MM-DD'))
        $("#endDate").val(picker.endDate.format('YYYY-MM-DD'))

        var params = {
            startDate:picker.startDate.format('YYYY-MM-DD'),
            endDate:picker.endDate.format('YYYY-MM-DD'),
            peticion : 0,
            } 

        resumen_por_fecha(params)//PETICION AL SERVER

    })

}

function resumen_por_fecha(params){
 //===== 1ERA PETICIÓN AXIOS =====\\
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/dinamic-api?format=json',conf)
    .then(function(response){
    	var $data = response.data

    	if($data.results === true){
    		if($data.nro_servicios > 0){
    			color = '#3b5998'
    		}else{
    			color = 'red'
    		}
    		$("#primer_resumen").html('<h6 class="text-center" style="color:'+ color+'">' + $data.nro_elementos + ' | ' + $data.recaudacion + ' recaudación estimada.</h6>')

    		charge_option(1) // cargo el primer selector

    	}else{
    		_info($data.errorMsj)
    	}


    })
    .then(function(){

    })
    .catch(function(error){
    	handleErrorAxios(error)
    })
}


function charge_option(nro_selector){

	var data = [
		{'id':'todos','text':'..:: Opción ::..'},
		{'id':'commune','text':'Comuna'},
		{'id':'node_code','text':'Nodos'},
		{'id':'service_name','text':'Planes'},
		{'id':'get_status_display','text':'Status'},
		{'id':'document_type','text':'Tipo de Documento'},
		{'id':'seller_name','text':'Vendedor'},
	]

	var html = ''
	//html += '<option value="todos">..:: Selección ::..</option>'

	for(var i = 0;i<data.length;i++){
		if(nro_selector == 1)
		{
			html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>';

		}
		else if(nro_selector == 2)
		{
			if(data[i]['id'] !== $("#opcionA").val()){
				html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>'
			}
		}
		else if(nro_selector == 3)
		{
			if((data[i]['id'] !== $("#opcionA").val()) && (data[i]['id'] !== $("#opcionB").val())){
				html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>'
			}
		}
	}

	if(nro_selector == 1){
		$("#opcionA").html(html)
	}else if(nro_selector == 2){
		$("#opcionB").html(html)
	}else if(nro_selector == 3){
		$("#opcionC").html(html)
	}

}


$("#opcionA").on('change', function(){

	var opcionA = $(this).val();
	if(opcionA !== 'todos'){

		$("#eleccionA,#eleccionB,#eleccionC").empty()
		$("#opcionB,#opcionC").val('todos').trigger('change')

		var nombre = $("#opcionA option:selected").text()
		$(".labelA, ._A").text(nombre)

		var params = {	
			startDate : $("#startDate").val(),
			endDate : $("#endDate").val(),
			opcionA : opcionA,
			peticion : 1,
		}

		peticion_one(params)
	}else{
		$("#eleccionA,#eleccionB,#eleccionC").empty()
		$("#opcionB,#opcionC").val('todos').trigger('change')
		$(".labelA, ._A").text('..:: Opción ::..')
	}

})

function peticion_one(params){
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/dinamic-api?format=json',conf)
    .then(function(response){
    	var $data = response.data

    	if($data.results === true){
    		
    		json_select2.data = $data.eleccionA
    		json_select2.templateSelection = function (data, container) {
		      $(container).css({
		      	"background-color":'#3b5998',
		      	"color":'white'
		      })
		      
		      return data.text
		    }

    		$("#eleccionA").select2(json_select2)

    		charge_option(2) // cargo el primer selector

    	}else{
    		_info($data.errorMsj)
    	}


    })
    .then(function(){

    })
    .catch(function(error){
    	handleErrorAxios(error)
    })
}

$("#eleccionA").on('change',function(){

    var eleccionA = $(this).val()

    if(eleccionA.length !== 0){
        $("#opcionB").prop('disabled',false)
    }else{
        $("#opcionB").prop('disabled',false)
    }

})


$("#opcionB").on('change', function(){

	var opcionA = $("#opcionA").val();
	var eleccionA = $("#eleccionA").val();

	var opcionB = $(this).val();

	if(opcionB !== 'todos'){
		$("#eleccionB,#eleccionC").empty()

		var nombre = $("#opcionB option:selected").text()
		$(".labelB, ._B").text(nombre)

		var params = {	
			startDate : $("#startDate").val(),
			endDate : $("#endDate").val(),
			opcionA : opcionA,
			eleccionA : JSON.stringify(eleccionA),
			opcionB : opcionB,
			peticion : 2,
		}

		peticion_two(params)
	}else{
		$("#eleccionB,#eleccionC").empty()
		$("#opcionC").val('todos').trigger('change')
		$(".labelB, ._B").text('..:: Opción ::..')
	}

})


function peticion_two(params){
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/dinamic-api?format=json',conf)
    .then(function(response){
    	var $data = response.data

    	if($data.results === true){
    		
    		json_select2.data = $data.eleccionB
    		json_select2.templateSelection = function (data, container) {
		      $(container).css({
		      	"background-color":'#3b5998',
		      	"color":'white'
		      })
		      
		      return data.text
		    }

    		$("#eleccionB").select2(json_select2)

    		charge_option(3) // cargo el primer selector

    	}else{
    		_info($data.errorMsj)
    	}


    })
    .then(function(){

    })
    .catch(function(error){
    	handleErrorAxios(error)
    })
}


$("#eleccionB").on('change',function(){

    var eleccionB = $(this).val()

    if(eleccionB.length !== 0){
        $("#opcionC").prop('disabled',false)
    }else{
        $("#opcionC").prop('disabled',false)
    }

})


$("#opcionC").on('change', function(){

	var opcionA = $("#opcionA").val();
	var eleccionA = $("#eleccionA").val();
	var opcionB = $("#opcionB").val();
	var eleccionB = $("#eleccionB").val();

	var opcionC = $(this).val();
	if(opcionC !== 'todos'){
		$("#eleccionC").empty()
		var nombre = $("#opcionC option:selected").text()
		$(".labelC, ._C").text(nombre)

		var params = {	
			startDate : $("#startDate").val(),
			endDate : $("#endDate").val(),
			opcionA : opcionA,
			eleccionA : JSON.stringify(eleccionA),
			opcionB : opcionB,
			eleccionB : JSON.stringify(eleccionB),
			opcionC : opcionC,
			peticion : 3,
		}

		peticion_three(params)
	}else{
		$("#eleccionC").empty()
		$(".labelC, ._C").text('..:: Opción ::..')
	}

})


function peticion_three(params){
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/dinamic-api?format=json',conf)
    .then(function(response){
    	var $data = response.data

    	if($data.results === true){

    		json['eleccionC'] = $data.eleccionC;
    		
    		json_select2.data = $data.eleccionC
    		json_select2.templateSelection = function (data, container) {
		      $(container).css({
		      	"background-color":'#3b5998',
		      	"color":'white'
		      })
		      
		      return data.text
		    }

    		$("#eleccionC").select2(json_select2)


    	}else{
    		_info($data.errorMsj)
    	}


    })
    .then(function(){

    })
    .catch(function(error){
    	handleErrorAxios(error)
    })
}

$("#eleccionC").on('change', function(){
   var eleccion = $(this).val()
    var lista = [];
     var lista2 = [];
    var eleccionesC = json['eleccionC']//IMPORTANTE PARA EL CONTADOR


    for(i=0;i<eleccion.length;i++){
        for(j=0;j<eleccionesC.length;j++){
            if(eleccion[i]==eleccionesC[j]['id']){
                lista.push(eleccionesC[j]['nro_servicios'])
                lista2.push(eleccionesC[j]['recaudacion'])
            }
        }

    }
    var resumen = lista.reduce((a, b) => a + b, 0)
    var resumen2 = lista2.reduce((a, b) => a + b, 0)



    if(resumen==0){
        color = 'text-center'
        col = '#b71c1c';
    }else if((resumen > 0) && (resumen < 500)){
        color = 'text-center'
        col = '#4db6ac'
    }else if(resumen >= 500){
        color = 'text-center'
        col = '#3b5998'
    }

    $("#resumen").html('<h5 class="'+color+'" style="color:'+col+';">'+SeparateNumber(resumen)+' Servicios | $ '+ SeparateNumber(resumen2) +' Recaudación</h5>')

    
})


$("#btn_reset").on('click',function(){

	/* seteamos los valores de los selectores */
	$("#opcionA,#opcionB,#opcionC")
	.html('<option value="todos">..:: Opción ::..</option>');

	$("#filtroA,#filtroB,#filtroC")
	.html('<option value="todos">..:: Filtro ::..</option>');

	$("#eleccionA,#eleccionB,#eleccionC,#chartdiv,#chartdiv2").empty();

	/* vaciamos el resumen del span*/
	$("#primer_resumen,#resumen")
	.html('<h5 class="text-center text-white"></h5>');
	

	$("#tab_grafico").trigger('click');

	$("#card2,#tab2,#tab3").addClass('filtros');


})


$("#btn_play").on('click', function(){

	var eleccionA = $("#eleccionA").val();
	var eleccionB = $("#eleccionB").val();
	var eleccionC = $("#eleccionC").val();

	if((eleccionA.length == 0) || (eleccionC.length == 0) || (eleccionC.length == 0)){
		_info('No se puede enviar información sin contenido en los selectores.')
	}else{
		var params = {
			startDate:$("#startDate").val(),
			endDate:$("#endDate").val(),
			opcionA:$("#opcionA").val(),
			opcionB:$("#opcionB").val(),
			opcionC:$("#opcionC").val(),
			eleccionA:JSON.stringify(eleccionA),
			eleccionB:JSON.stringify(eleccionB),
			eleccionC:JSON.stringify(eleccionC),
			peticion:4
		}
		
		$("#tab_grafico").trigger('click');
		$("#chartdiv,#chartdiv2").empty();
		
		peticion_four(params)
	}

})

function peticion_four(params){
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/dinamic-api?format=json',conf)
    .then(function(response){
    	var $data = response.data
    	
    	

    	if($data.results){

    		$.when(

    			SankeyGraph($data.sankey),

	    		$("#filtroA").select2({
	    			data:$data.filtroA
	    		}),

	    		$("#filtroB").select2({
	    			data:$data.filtroB
	    		}),

	    		$("#filtroC").select2({
	    			data:$data.filtroC
	    		}),

                $("#filtroD").select2({
                    data:$data.fechas
                }),


	    		tableBasic($data),

                GraficoFechas($data.fechas,'fecha_activacion','chartdiv2'),

	    		tableDetail($data),

			)
			.then(function(){

				$("#container").removeClass('filtros');
				$("#collapseOne").removeClass('show');
				$("#card2,#tab2,#tab3").removeClass('filtros');
				$("#collapseTwo").addClass('show');

			})

    	}else{
    		_info($data.errorMsj)
    	}


    })
    .catch(function(error){
    	handleErrorAxios(error)
    })	
}



function tableBasic(data){

	var $data = data
//========================== BEGIN TABLA BASICA ==========================\\
   var tableA = new myTable();

   	tableA.columns = ['opcionA',
   					'opcionB',
   					'opcionC',
   					'nro_servicios',
   					'recaudacion',
   					'promedio',
   					'mediana',
   					'maximo',
   					'minimo']
   	tableA.palabraClave = 'Registros'
   	tableA.order_by_columns = [2,'desc']

   	jsonA = tableA.render_table($data.tableBasic)
   	
    
    jsonA['columns'][3] = {'data':'nro_servicios','className':'text-center'}
   	jsonA['columns'][4] = {'data':'recaudacion','className':'text-center'}
		jsonA['columns'][5] = {'data':'promedio','className':'text-center'}
    jsonA['columns'][6] = {'data':'mediana','className':'text-center'}
   	jsonA['columns'][7] = {'data':'maximo','className':'text-center'}
		jsonA['columns'][8] = {'data':'minimo','className':'text-center'}

   	jsonA['columnDefs'].push({
   						"render": function (data, type, row) {
                           return '<b><i>'+data +'</i></b>'
                       },
                       "targets": [0,1,2]
                       })

  	jsonA['columnDefs'].push(
                      {"render": function (data, type, row) {
                              return '<b><i>' +SeparateNumber(data)+'</i></b>';
                      },
                      "targets": [3,6]
					  })
					  
  	jsonA['columnDefs'].push(
                      {
                      "render": function (data, type, row) {
                              return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                          },
                      "targets": [4,5,7]
                      })

    jsonA['columnDefs'].push(
                      {

                      "render": function (data, type, row) {
                              return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                          },
                      "targets": [8],
                      "orderable":true,
                      })


    jsonA["footerCallback"] =  function ( row, data, start, end, display ) {
       var api = this.api(), data;

       // Remove the formatting to get integer data for summation
       var intVal = function ( i ) {
           return typeof i === 'string' ?
               i.replace(/[\$,]/g, '')*1 :
               typeof i === 'number' ?
                   i : 0;
       };

       size = api
           .column( 3 )
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );
      
              
       sizeDetail = api
           .column( 3 ,{page:'current'})
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

       // Total over all pages
       totalDetail = api
           .column( 4, {page:'current'})
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

        total = api
           .column( 4 )
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

       // Update footer
       $( api.column( 3 ).footer() ).html(
           '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
       );
       $( api.column( 4 ).footer() ).html(
           '<b>$'+ SeparateNumber(totalDetail) + ' | $ ' + SeparateNumber(total) + 
           '</b>'
       );
   }

    $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          if ( settings.nTable.id !==  'example') {
          return true;
          }
          var $select_dia = $("#filtroA").val();
          var dt_annio = data[0];

          if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
          {
            return true;
          }

          return false;
        });

    $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          if ( settings.nTable.id !==  'example') {
          return true;
          }
          var $select_dia = $("#filtroB").val();
          var dt_annio = data[1];

          if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
          {
            return true;
          }

          return false;
        });

    $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          if ( settings.nTable.id !==  'example') {
          return true;
          }
          var $select_dia = $("#filtroC").val();
          var dt_annio = data[2];

          if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
          {
            return true;
          }

          return false;
        });


  	var bt = $("#example").DataTable(jsonA)
	$("#filtroA,#filtroB,#filtroC").change(function(){
        bt.draw()
      })

//========================== END TABLA BASICA ==========================\\


}


function tableDetail(data){

	var $data = data	
//========================== BEGIN TABLA CLIENTES ==========================\\

    var tableB = new myTable();

    tableB.columns = ['opcionA',
    				'opcionB',
    				'opcionC',
    				'cliente_name',
    				'cliente_rut',
                    'nro_contrato',
                    'direccion',
                    'fecha_activacion',
                    'nro_servicios',
                    'recaudacion',
                    'promedio',
                    'botones']
    tableB.palabraClave = 'Clientes'
    tableB.order_by_columns = [1,'asc'],[9,'desc']
    jsonB = tableB.render_table($data.tableDetail)
    
    jsonB['columns'][3] = {'data':'cliente_name','className':'text-nowrap'}
    jsonB['columns'][5] = {'data':'nro_contrato','className':'text-center'}
    jsonB['columns'][7] = {'data':'fecha_activacion','className':'text-center'}

    jsonB['columns'][8] = {'data':'nro_servicios','className':'text-center'}
    jsonB['columns'][9] = {'data':'recaudacion','className':'text-center'}

    jsonB['columnDefs'].push(
                     {"visible":false,
                     "targets": [0,1,2],
                     "searchable":true,
                     'orderable':true,
                     "render": function (data, type, row) {
                         return '<b><i>'+data +'</i></b>'},
                     })

    jsonB['columnDefs'].push(
                           {"render": function (data, type, row) {
                               return '<b><i>'+ data +'</i></b>'},
                           "targets": [3,4,5,6,7]
                           })	       

    jsonB['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>' + SeparateNumber(data) + '</i></b>';
                              },
                          "targets": [8]
                          })
    jsonB['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                              },
                          "targets": [9,10]
                          })

    jsonB["footerCallback"] =  function ( row, data, start, end, display ) {
               var api = this.api(), data;
    
               // Remove the formatting to get integer data for summation
               var intVal = function ( i ) {
                   return typeof i === 'string' ?
                       i.replace(/[\$,]/g, '')*1 :
                       typeof i === 'number' ?
                           i : 0;
               };
   
               size = api
                   .column( 8 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
              
                      
               sizeDetail = api
                   .column( 8 ,{page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
    
               // Total over all pages
               totalDetail = api
                   .column( 9, {page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );

                total = api
                   .column( 9 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
    
               // Update footer
               $( api.column( 8 ).footer() ).html(
                   '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
               );
               $( api.column( 9 ).footer() ).html(
                   '<b>$'+ SeparateNumber(totalDetail) + ' | $ ' + SeparateNumber(total) + 
                   '</b>'
               );
           }

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroA").val();
      var dt_annio = data[0];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroB").val();
      var dt_annio = data[1];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroC").val();
      var dt_annio = data[2];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroD").val();
      var dt_annio = data[7];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );

    var groupColumn = [1];

    jsonB["drawCallback"] = function ( settings ) {
        var api = this.api();
        var rows = api.rows( {page:'current'} ).nodes();
        var last=null; 
        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
            if ( last !== group ) {
                $(rows).eq( i ).before(
                    '<tr class="group text-nowrap"><td colspan="12" style="color:#3b5998"><b><i>'+group.toUpperCase()+'</i></b></td></tr>'
                );

                last = group;
            }
        } );
       }


  	var dt = $("#example2").DataTable(jsonB)

  	$("#filtroA,#filtroB,#filtroC,#filtroD").change(function(){
	    dt.draw()
    })

//========================== END TABLA CLIENTES ==========================\\


}

