userInSession();
App.setPageTitle('Águila | Clientes');
titlePage('Clientes - General')

var json = {};

$(document).ready(function(){

    
 

    $.when(
        dateRange("#default-daterange"),
        
    )
    .then(function(){
        $("#container_ppal").removeClass('filtros')
    })
    

})


    $(".select2").select2({
        width:'100%',
        'language': {
            errorLoading:function(){
                return Capitalize(HDD.get('username')) + ", no se pudieron cargar los resultados"
            },
            inputTooLong:function(e){
                var n=e.input.length-e.maximum,r="Por favor, elimine "+n+" car";return r+=1==n?"ácter":"acteres"
            },
            inputTooShort:function(e){
                var n=e.minimum-e.input.length,r="Por favor, introduzca "+n+" car";return r+=1==n?"ácter":"acteres"
            },
            loadingMore:function(){
                return Capitalize(HDD.get('username')) + ", estamos cargando más resultados…"
            },
            maximumSelected:function(e){
                var n=Capitalize(HDD.get('username')) + ", sólo puede seleccionar "+e.maximum+" elemento";
                return 1!=e.maximum&&(n+="s"),n
            },
            noResults:function(){
                return Capitalize(HDD.get('username')) + ", no se encontraron resultados"
            },
            searching:function(){
                return Capitalize(HDD.get('username')) + ", estamos buscando…"
            },
            removeAllItems:function(){
                return"Eliminar todos los elementos"
            }
        },
    })





// VARIABLE DE ENTORNO GLOBAL

var opciones = [
            {'id':'commune','text':'Comuna'},
            {'id':'node_code','text':'Nodos'},
            {'id':'service_name','text':'Planes'},
            {'id':'get_status_display','text':'Status'},
            {'id':'document_type','text':'Tipo de Documento'},
            {'id':'seller_name','text':'Vendedor'},
        ]



/* ================= BEGIN MOVIMIENTOS DE LAS FECHAS ================= */

function dateRange(id){
    var start = moment().subtract(29, 'days');
    var end = moment();

    var quarter = moment().quarter();
    // Aquí se inicializa el selector de fechas
    $(id + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    //===== TRIMESTRES =====\\
    /******** FORMATO AAAA-MM-DD ********/
    var Q1A = moment().year().toString() + '-01-01'
    var Q1B = moment().year().toString() + '-03-31'

    var Q2A = moment().year().toString() + '-04-01'
    var Q2B = moment().year().toString() + '-06-30'

    var Q3A = moment().year().toString() + '-07-01'
    var Q3B = moment().year().toString() + '-09-30'

    var Q4A = moment().year().toString() + '-10-01'
    var Q4B = moment().year().toString() + '-12-31'

    //===== SEMESTRE =====\\

    var S1A = moment().year().toString() + '-01-01'
    var S1B = moment().year().toString() + '-06-30'

    var S2A = moment().year().toString() + '-07-01'
    var S2B = moment().year().toString() + '-12-31'

    $(id).daterangepicker({
        startDate: start,
        endDate: end,
        showDropdowns: true,
        minDate: '01/01/2001',
        maxDate: '01/01/2099',
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-warning',
        cancelClass: 'btn-inverse',
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Enviar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Seleccionar Fecha',
            todayLabel: 'Hoy',
            daysOfWeek: [
            "Lun",
            "Mar",
            "Mié",
            "Jue",
            "Vie",
            "Sáb",
            "Dom"],
            monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
            ],
            firstDay: 0
        },
        ranges: {
        'Hoy': [moment(), moment()],
        'Mes actual': [moment().startOf('month'), moment().endOf('month')],
        'Últimos 15 días': [moment().subtract(14, 'days'), moment()],
        'Últimos 30 días': [moment().subtract(29, 'days'), moment()],

        'Trimestre 1': [moment(Q1A,'YYYY-MM-DD'), moment(Q1B,'YYYY-MM-DD')],
        'Trimestre 2': [moment(Q2A,'YYYY-MM-DD'), moment(Q2B,'YYYY-MM-DD')],
        'Trimestre 3': [moment(Q3A,'YYYY-MM-DD'), moment(Q3B,'YYYY-MM-DD')],
        'Trimestre 4': [moment(Q4A,'YYYY-MM-DD'), moment(Q4B,'YYYY-MM-DD')],

        'Semestre 1': [moment(S1A,'YYYY-MM-DD'), moment(S1B,'YYYY-MM-DD')],
        'Semestre 2': [moment(S2A,'YYYY-MM-DD'), moment(S2B,'YYYY-MM-DD')],

        'Mes actual': [moment().startOf('month'), moment().endOf('month')],
        'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Año anterior': [moment().subtract(364, 'days'), moment()],//
        }
        });

    //===== PARAMETROS PARA PETICIONES AJAX O AXIOS =====\\

    $(id).on('apply.daterangepicker', function(ev, picker) {
        /*
            MANEJADOR DE EVENTOS PROPIOS DEL DETERANGE, INICIA 
            CUANDO PULSAMOS EL BTN ACEPTAR(APPLY) DEL CALENDARIO
            DESPLEGADO.
        */

        ev.preventDefault()
        var params = {
            startDate:picker.startDate.format('YYYY-MM-DD'),
            endDate:picker.endDate.format('YYYY-MM-DD'),
            peticion : 0,
            } 
        $("#startDate").val(picker.startDate.format('YYYY-MM-DD'));
        $("#endDate").val(picker.endDate.format('YYYY-MM-DD'))

        resumen_por_fecha(params)//PETICION AL SERVER

    })

}

function resumen_por_fecha(params){

    /*
        ESTA FUNCIÓN REALIZA UN PETICION PARA CALCULAR EL NUMERO DE CONTRATOS QUE SE HAN
        GENERADO EN EL RANGO DE FECHAS SELECCIONADO.
    */

    //===== 1ERA PETICIÓN AXIOS =====\\
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    axios.get('api/v1/selector-api?format=json',conf)
    //axios.get('pruebas/selector-api?format=json',conf)
    .then(function(response){
        var $data = response.data

        if($data.results == true){

            if(conf.params.peticion == 0){

                if($data.nro_servicios == 0){
                    col = '#b71c1c'
                }else{
                    col = '#3b5998'

                    $("#opcionA").select2({
                        width:'100%',
                        data:[
                            {'id':'commune','text':'Comuna'},
                            {'id':'node_code','text':'Nodos'},
                            {'id':'service_name','text':'Planes'},
                            {'id':'get_status_display','text':'Status'},
                            {'id':'document_type','text':'Tipo de Documento'},
                            {'id':'seller_name','text':'Vendedor'},
                        ]
                    })

                }

                $("#primer_resumen").html('<h5 class="text-center" style="color:'+col+';">Nro de servicios para este rango de fecha: '+SeparateNumber($data.nro_elementos)+'</h5>')
            
        }else if(conf.params.peticion == 1){

            $("#eleccionA").empty().select2({
                data:$data.eleccionA,
                'language': {
                    errorLoading:function(){
                        return Capitalize(HDD.get('username')) + ", no se pudieron cargar los resultados"
                    },
                    inputTooLong:function(e){
                        var n=e.input.length-e.maximum,r="Por favor, elimine "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    inputTooShort:function(e){
                        var n=e.minimum-e.input.length,r="Por favor, introduzca "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    loadingMore:function(){
                        return Capitalize(HDD.get('username')) + ", estamos cargando más resultados…"
                    },
                    maximumSelected:function(e){
                        var n=Capitalize(HDD.get('username')) + ", sólo puede seleccionar "+e.maximum+" elemento";
                        return 1!=e.maximum&&(n+="s"),n
                    },
                    noResults:function(){
                        return Capitalize(HDD.get('username')) + ", no se encontraron resultados"
                    },
                    searching:function(){
                        return Capitalize(HDD.get('username')) + ", estamos buscando…"
                    },
                    removeAllItems:function(){
                        return"Eliminar todos los elementos"
                    }
                },

                'templateSelection': function (data, container) {
                      $(container).css({"background-color":'#3b5998',"color":'white'})
                      
                      return data.text
                    }
            

            })



        }else if(conf.params.peticion == 2){
            $("#eleccionB").empty()

            $("#eleccionB").select2({
                data:$data.eleccionB,
                'language': {
                    errorLoading:function(){
                        return Capitalize(HDD.get('username')) + ", no se pudieron cargar los resultados"
                    },
                    inputTooLong:function(e){
                        var n=e.input.length-e.maximum,r="Por favor, elimine "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    inputTooShort:function(e){
                        var n=e.minimum-e.input.length,r="Por favor, introduzca "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    loadingMore:function(){
                        return Capitalize(HDD.get('username')) + ", estamos cargando más resultados…"
                    },
                    maximumSelected:function(e){
                        var n=Capitalize(HDD.get('username')) + ", sólo puede seleccionar "+e.maximum+" elemento";
                        return 1!=e.maximum&&(n+="s"),n
                    },
                    noResults:function(){
                        return Capitalize(HDD.get('username')) + ", no se encontraron resultados"
                    },
                    searching:function(){
                        return Capitalize(HDD.get('username')) + ", estamos buscando…"
                    },
                    removeAllItems:function(){
                        return"Eliminar todos los elementos"
                    }
                },

                'templateSelection': function (data, container) {
                      $(container).css({"background-color":'#3b5998',"color":'white'})
                      
                      return data.text
                    }
            

            })

        }else if(conf.params.peticion == 3){
            $("#eleccionC").empty()

            json['eleccionC'] = $data.eleccionC;

            $("#eleccionC").select2({
                data:$data.eleccionC,
                'language': {
                    errorLoading:function(){
                        return Capitalize(HDD.get('username')) + ", no se pudieron cargar los resultados"
                    },
                    inputTooLong:function(e){
                        var n=e.input.length-e.maximum,r="Por favor, elimine "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    inputTooShort:function(e){
                        var n=e.minimum-e.input.length,r="Por favor, introduzca "+n+" car";return r+=1==n?"ácter":"acteres"
                    },
                    loadingMore:function(){
                        return Capitalize(HDD.get('username')) + ", estamos cargando más resultados…"
                    },
                    maximumSelected:function(e){
                        var n=Capitalize(HDD.get('username')) + ", sólo puede seleccionar "+e.maximum+" elemento";
                        return 1!=e.maximum&&(n+="s"),n
                    },
                    noResults:function(){
                        return Capitalize(HDD.get('username')) + ", no se encontraron resultados"
                    },
                    searching:function(){
                        return Capitalize(HDD.get('username')) + ", estamos buscando…"
                    },
                    removeAllItems:function(){
                        return"Eliminar todos los elementos"
                    }
                },

                'templateSelection': function (data, container) {
                      $(container).css({"background-color":'#3b5998',"color":'white'})
                      
                      return data.text
                    }
            

            })
        }
            



        }else{
            _info($data.errorMsj)
        }

    })
    .then(function(){

    })
    .catch(function(e){
        handleErrorAxios(e)
    })
}

/* ================= END MOVIMIENTOS DE LAS FECHAS ================= */


function charge_option(nro_selector){

    var data = [
        {'id':'todos','text':'..:: Opción ::..'},
        {'id':'commune','text':'Comuna'},
        {'id':'node_code','text':'Nodos'},
        {'id':'service_name','text':'Planes'},
        {'id':'get_status_display','text':'Status'},
        {'id':'document_type','text':'Tipo de Documento'},
        {'id':'seller_name','text':'Vendedor'},
    ]

    var html = ''
    //html += '<option value="todos">..:: Selección ::..</option>'

    for(var i = 0;i<data.length;i++){
        if(nro_selector == 1)
        {
            html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>';

        }
        else if(nro_selector == 2)
        {
            if(data[i]['id'] !== $("#opcionA").val()){
                html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>'
            }
        }
        else if(nro_selector == 3)
        {
            if((data[i]['id'] !== $("#opcionA").val()) && (data[i]['id'] !== $("#opcionB").val())){
                html += '<option value="'+data[i]['id']+'">'+data[i]['text']+'</option>'
            }
        }
    }

    if(nro_selector == 1){
        $("#opcionA").html(html)
    }else if(nro_selector == 2){
        $("#opcionB").html(html)
    }else if(nro_selector == 3){
        $("#opcionC").html(html)
    }

}


/* ================= BEGIN MOVIMIENTOS DE LOS SELECTORES ================= */

$("#opcionA").on('change', function(){

    var opcionA = $(this).val();

    if(opcionA !== 'todos'){

        var params = {
            startDate:$("#startDate").val(),//VALORES DE UN INPUT HIDDEN
            endDate:$("#endDate").val(),//VALORES DE UN INPUT HIDDEN
            opcionA : opcionA,
            peticion:1
        }

        resumen_por_fecha(params)
        
    }else {
        
        $("#eleccionA").empty()
        $("#opcionB").val('todos').trigger('change');

            /* vaciamos el resumen del span*/
            $("#primer_resumen")
            .html('<h6 class="text-center text-white"></h6>')

    }

    $(".labelA, ._A").text($('#opcionA option:selected').text().replace('..:: ','').replace(' ::..','').replace('Opción','Opción A'))

});


$("#opcionB").on('change', function(){

    var opcionA = $("#opcionA").val();
    var opcionB = $(this).val();
    var eleccionA = JSON.stringify($("#eleccionA").val())

    if(opcionB !== 'todos'){
        var params = {
            startDate:$("#startDate").val(),//VALORES DE UN INPUT HIDDEN
            endDate:$("#endDate").val(),//VALORES DE UN INPUT HIDDEN
            opcionA : opcionA,
            eleccionA:eleccionA,
            opcionB : opcionB,
            peticion:2
        }

        resumen_por_fecha(params)

    }else if(opcionB == 'todos'){

        $("#eleccionB,#eleccionC").empty()
        $("#opcionC").val('todos').trigger('change')
    }

    $(".labelB, ._B").text($('#opcionB option:selected').text().replace('..:: ','').replace(' ::..','').replace('Opción','Opción B'))

})


$("#opcionC").on('change', function(){
    var opcionC = $(this).val()

    var opcionA = $("#opcionA").val();
    var opcionB = $("#opcionB").val();
    var eleccionA = JSON.stringify($("#eleccionA").val())
    var eleccionB = JSON.stringify($("#eleccionB").val())

    if(opcionC !== 'todos'){

        var params = {
            startDate:$("#startDate").val(),//VALORES DE UN INPUT HIDDEN
            endDate:$("#endDate").val(),//VALORES DE UN INPUT HIDDEN
            opcionA : opcionA,
            eleccionA:eleccionA,
            opcionB : opcionB,
            eleccionB:eleccionB,
            opcionC : opcionC,
            peticion:3
        }

        resumen_por_fecha(params)

    }else{
        $("#eleccionC").empty()
    }

    $(".labelC, ._C").text($('#opcionC option:selected').text().replace('..:: ','').replace(' ::..','').replace('Opción','Opción C'))
})




                
$("#eleccionA").on('change', function(){
    var eleccionA = $(this).val()

    if(eleccionA.length !== 0){

        charge_option(2)
    }else{
        $("#opcionB,#opcionC").val('todos').trigger('change')
        $("#eleccionB,#eleccionC").empty()
    }
})

$("#eleccionB").on('change', function(){
    var eleccionB = $("#eleccionB").val()
    
    if(eleccionB.length !== 0){

        charge_option(3)
    }else{
        $("#opcionC").val('todos').trigger('change')
        $("#eleccionC").empty()
        
    }
})

$("#eleccionC").on('change', function(){
   var eleccion = $(this).val()
    var lista = [];
     var lista2 = [];
    var eleccionesC = json['eleccionC']//IMPORTANTE PARA EL CONTADOR


    for(i=0;i<eleccion.length;i++){
        for(j=0;j<eleccionesC.length;j++){
            if(eleccion[i]==eleccionesC[j]['id']){
                lista.push(eleccionesC[j]['nro_servicios'])
                lista2.push(eleccionesC[j]['recaudacion'])
            }
        }

    }
    var resumen = lista.reduce((a, b) => a + b, 0)
    var resumen2 = lista2.reduce((a, b) => a + b, 0)



    if(resumen==0){
        color = 'text-center'
        col = '#b71c1c';
    }else if((resumen > 0) && (resumen < 500)){
        color = 'text-center'
        col = '#4db6ac'
    }else if(resumen >= 500){
        color = 'text-center'
        col = '#3b5998'
    }

    $("#resumen").html('<h5 class="'+color+'" style="color:'+col+';">'+SeparateNumber(resumen)+' Servicios | $ '+ SeparateNumber(resumen2) +' Recaudación</h5>')

    
})

/* ================= END MOVIMIENTOS DE LOS SELECTORES ================= */

$("#btn_reset").on('click',function(){

    /* seteamos los valores de los selectores */
    $("#opcionA,#opcionB,#opcionC")
    .html('<option value="todos">..:: Opción ::..</option>');

    $("#filtroA,#filtroB,#filtroC")
    .html('<option value="todos">..:: Filtro ::..</option>');

    $("#eleccionA,#eleccionB,#eleccionC,#chartdiv").empty();

    /* vaciamos el resumen del span*/
    $("#primer_resumen,#resumen")
    .html('<h5 class="text-center text-white"></h5>');
    

    $("#tab_grafico").trigger('click');

    $("#card2,#tab2,#tab3").addClass('filtros');
    $("#startDate,#endDate").val('').empty();

})



$("#btn_play").on('click', function(){
    var eleccionA = $("#eleccionA").val();
    var eleccionB = $("#eleccionB").val();
    var eleccionC = $("#eleccionC").val();

    if((eleccionA.length == 0) || (eleccionC.length == 0) || (eleccionC.length == 0)){
        _info('No se puede enviar información sin contenido en los selectores.')
    }else{


        var params = {
            startDate:$("#startDate").val(),
            endDate:$("#endDate").val(),
            opcionA:$("#opcionA").val(),
            opcionB:$("#opcionB").val(),
            opcionC:$("#opcionC").val(),
            eleccionA:JSON.stringify(eleccionA),
            eleccionB:JSON.stringify(eleccionB),
            eleccionC:JSON.stringify(eleccionC),
            peticion:4
        }
        
        $("#tab_grafico").trigger('click');
        
        initial_data(params)
    }

})

// ============ END CARGA LOS SELECTORES BÁSICOS ============= \\





function initial_data(params){

    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
        };
    /*axios.get('api/v1/services-api?format=json',conf)*/
    axios.get('api/v1/selector-api?format=json',conf)
    .then(function(response){
        var $data = response.data

        $("#chartdiv").trigger('reset')
    //====================== BEGIN FILTROS DATATABLES ======================\\

        $("#filtroA").select2({
            width:'100%',
            placeholder:'..:: ' + $("#opcionA option:selected").text() + ' ::..',
            data:$data.filtroA
        })
        $("#filtroB").select2({
            width:'100%',
            placeholder:'..:: ' + $("#opcionB option:selected").text() + ' ::..',
            data:$data.filtroB
        })
        $("#filtroC").select2({
            width:'100%',
            placeholder:'..:: ' + $("#opcionC option:selected").text() + ' ::..',
            data:$data.filtroC
        })


    //====================== END FILTROS DATATABLES ======================\\



    //====================== BEGIN GRAFICOS SANKEY - SEMI CIRCULARES ======================\\


        /* LIMPIAMOS EL DIV DE UN GRÁFICO PREVIO */
        
        SankeyGraph($data.grafico)



    //====================== END GRAFICOS SANKEY - SEMI CIRCULARES ======================\\



    //========================== BEGIN TABLA BASICA ==========================\\
       var tableA = new myTable();

        tableA.columns = ['opcionA','opcionB','opcionC','nro_servicios',
                       'recaudacion','promedio','mediana','varianza','desviacion',
                        'maximo','minimo']
        tableA.palabraClave = 'Registros'
        tableA.order_by_columns = [3,'desc']

        jsonA = tableA.render_table($data.tableBasic)
        

        jsonA['columns'][1] = {'data':'opcionB','className':'text-nowrap'}
        jsonA['columns'][2] = {'data':'opcionC','className':'text-nowrap'}
        
        jsonA['columns'][3] = {'data':'nro_servicios','className':'text-center'}
        jsonA['columns'][4] = {'data':'recaudacion','className':'text-center'}
   
        jsonA['columnDefs'].push(
                           {"render": function (data, type, row) {
                               return '<b><i>'+data +'</i></b>'},
                           "targets": [0,1,2]
                           })
   
        var groupColumn = [1];

        jsonA['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>' +SeparateNumber(data)+'</i></b>';
                              },
                          "targets": [3,6]
                          })
                          
        jsonA['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                              },
                          "targets": [4,5,9,10]
                          })

        jsonA['columnDefs'].push(
                            {
                            "render": function (data, type, row) {
                                    return '<b><i>' +SeparateNumber(data,'d')+'</i></b>';
                                },
                            "targets": [7,8]
                            })


        $.fn.dataTable.ext.search.push(
          function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example') {
              return true;
              }
              var $select_dia = $("#filtroA").val();
              var dt_annio = data[0];

              if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
        );

        $.fn.dataTable.ext.search.push(
          function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example') {
              return true;
              }
              var $select_dia = $("#filtroB").val();
              var dt_annio = data[1];

              if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
        );

        $.fn.dataTable.ext.search.push(
          function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example') {
              return true;
              }
              var $select_dia = $("#filtroC").val();
              var dt_annio = data[2];

              if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
        );

        jsonA["footerCallback"] =  function ( row, data, start, end, display ) {
               var api = this.api(), data;
    
               // Remove the formatting to get integer data for summation
               var intVal = function ( i ) {
                   return typeof i === 'string' ?
                       i.replace(/[\$,]/g, '')*1 :
                       typeof i === 'number' ?
                           i : 0;
               };
   
               size = api
                   .column( 3 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
              
                      
               sizeDetail = api
                   .column( 3 ,{page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
                   
    
               // Total over all pages
               totalDetail = api
                   .column( 4, {page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );

                total = api
                   .column( 4 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
    
               // Update footer
               $( api.column( 3 ).footer() ).html(
                   '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
               );


               $( api.column( 4 ).footer() ).html(
                   '<b>$ '+ SeparateNumber(totalDetail) + ' | $ ' +SeparateNumber(total) + 
                   '</b>'
               );
           }

        var bt = $("#example").DataTable(jsonA)
        $("#filtroC,#filtroB,#filtroA").change(function(){
            bt.draw()
          })

    //========================== END TABLA BASICA ==========================\\
    
    
    //========================== BEGIN TABLA CLIENTES ==========================\\

        var tableB = new myTable();

        tableB.columns = ['opcionA','opcionB','opcionC','cliente_name','cliente_rut',
                            'nro_contrato','direccion',
                            'nro_servicios','recaudacion',
                            'promedio','botones']
        tableB.palabraClave = 'Clientes'
        tableB.order_by_columns = [8,'desc']
        jsonB = tableB.render_table($data.tableDetail)
        jsonB['columns'][3] = {'data':'cliente_name','className':'text-nowrap'}
        jsonB['columns'][5] = {'data':'nro_contrato','className':'text-center'}
        
        jsonB['columns'][7] = {'data':'nro_servicios','className':'text-center'}
        jsonB['columns'][8] = {'data':'recaudacion','className':'text-center'}

        jsonB['columnDefs'].push(
                         {"visible":false,
                         "targets": [0,1,2],
                         "searchable":true,
                         'orderable':true,
                         "render": function (data, type, row) {
                             return '<b><i>'+data +'</i></b>'},
                         })

        jsonB['columnDefs'].push(
                               {"render": function (data, type, row) {
                                   return '<b><i>'+ data +'</i></b>'},
                               "targets": [3,4,5,6]
                               })
       

        jsonB['columnDefs'].push(
                              {
                              "render": function (data, type, row) {
                                      return '<b><i>' + SeparateNumber(data) + '</i></b>';
                                  },
                              "targets": [7]
                              })
        jsonB['columnDefs'].push(
                              {
                              "render": function (data, type, row) {
                                      return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                                  },
                              "targets": [8,9]
                              })


        jsonB["footerCallback"] =  function ( row, data, start, end, display ) {
                   var api = this.api(), data;
        
                   // Remove the formatting to get integer data for summation
                   var intVal = function ( i ) {
                       return typeof i === 'string' ?
                           i.replace(/[\$,]/g, '')*1 :
                           typeof i === 'number' ?
                               i : 0;
                   };
       
                   size = api
                       .column( 7 )
                       .data()
                       .reduce( function (a, b) {
                           return intVal(a) + intVal(b);
                       }, 0 );
                  
                          
                   sizeDetail = api
                       .column( 7 ,{page:'current'})
                       .data()
                       .reduce( function (a, b) {
                           return intVal(a) + intVal(b);
                       }, 0 );
        
                   // Total over all pages
                   totalDetail = api
                       .column( 8, {page:'current'})
                       .data()
                       .reduce( function (a, b) {
                           return intVal(a) + intVal(b);
                       }, 0 );

                    total = api
                       .column( 8 )
                       .data()
                       .reduce( function (a, b) {
                           return intVal(a) + intVal(b);
                       }, 0 );
        
                   // Update footer
                   $( api.column( 7 ).footer() ).html(
                       '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
                   );
                   $( api.column( 8 ).footer() ).html(
                       '<b>$'+ SeparateNumber(totalDetail) + ' | $ ' + SeparateNumber(total) + 
                       '</b>'
                   );
               }

          $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example2') {
              return true;
              }
              var $select_dia = $("#filtroA").val();
              var dt_annio = data[0];

              if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
          );

          $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example2') {
              return true;
              }
              var $select_dia = $("#filtroB").val();
              var dt_annio = data[1];

              if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
          );

          $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
              if ( settings.nTable.id !==  'example2') {
              return true;
              }
              var $select_dia = $("#filtroC").val();
              var dt_annio = data[2];

              if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
              {
                return true;
              }

              return false;
            }
          );

            var groupColumn = [1];

            jsonB["drawCallback"] = function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null; 
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group text-nowrap"><td colspan="11" class="text-primary"><b><i>'+group.toUpperCase()+'</i></b></td></tr>'
                        );
     
                        last = group;
                    }
                } );
               }


          var dt = $("#example2").DataTable(jsonB)

              $("#filtroA,#filtroB,#filtroC").change(function(){
                dt.draw()
              })

    //========================== END TABLA CLIENTES ==========================\\

    })
    .then(function(){
        $("#container").removeClass('filtros')
        $("#collapseOne").removeClass('show')

        $("#card2,#tab2,#tab3").removeClass('filtros');

        $("#collapseTwo").addClass('show')
    })
    .catch(function(e){
        handleErrorAxios(e)
    })



}



