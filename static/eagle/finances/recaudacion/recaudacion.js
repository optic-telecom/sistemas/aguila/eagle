
	userInSession();
	App.setPageTitle('Águila | Finanzas');
	titlePage('Finanzas - Recaudación');

	//=========== SELECTOR DE FECHAS DATE PICKER ===========\\

	// SELECTOR BÁSICO
	$('#startDate,#endDate').datepicker({
		//todayHighlight: true,

		autoclose: true,
		language: 'es',
		format:'yyyy-mm-dd',
		orientation: 'bottom auto',

	});

	// SELECTOR POR TRIMESTRE - SEMESTRE - AÑO
	$('#annio_trimestre,#annio_semestre,#annio').datepicker({
		//todayHighlight: true,

		// === PARA MOSTRAR SOLO AÑOS ===\\
		autoclose: true,
		language: 'es',
	    format: "yyyy",
	    viewMode: "years", 
	    minViewMode: "years",
		orientation: 'bottom auto',

	});

	$("#trimestre").select2({
		width:'100%',
		data:[
			{'id':0,'text':'..:: Trimestre ::..'},
			{'id':1,'text':'Trimestre I (Q1)'},
			{'id':2,'text':'Trimestre II (Q2)'},
			{'id':3,'text':'Trimestre III (Q3)'},
			{'id':4,'text':'Trimestre IV (Q4)'},
			],
		placeholder:'..:: Trimestre ::..',

	})

	$("#semestre").select2({
		width:'100%',
		data:[
			{'id':0,'text':'..:: Semestre ::..'},
			{'id':1,'text':'Semestre I (S1)'},
			{'id':2,'text':'Semestre II (S2)'},
			],
		placeholder:'..:: Semestre ::..',

	})


	//=========== CAPTURA DEL VALOR DEL SELECTOR DE FECHAS ===========\\


	$("#rangeDate").click(function(e){
		e.preventDefault();

		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();

		if((startDate!='')&&(endDate!='')){
			$("#tab_graph").trigger('click');
			get_pagos(startDate,endDate,'general')
		}

	})



	// =================== TRIMESTRES POR VALOR DEL SELECTOR ========================= \\


	$("#rangeTrimestre").click(function(e){
		e.preventDefault();

		var annio_trimestre = $("#annio_trimestre").val();
		var trimestre = $("#trimestre").val();

		if((trimestre!=0)&&(annio_trimestre!='')){
			$("#tab_graph_trim").trigger('click');
			get_pagos_periodos('trimestre',annio_trimestre,trimestre);
		}

	})

	// =================== SEMESTRE POR VALOR DEL SELECTOR ========================= \\

	$("#rangeSemestre").click(function(e){
		e.preventDefault();

		var annio_semestre = $("#annio_semestre").val();
		var semestre = $("#semestre").val();

		if((semestre!=0)&&(annio_semestre!='')){
			$("#tab_graph_sem").trigger('click');
			get_pagos_periodos('semestre',annio_semestre,semestre);
		}

	})

	// =================== AÑO POR VALOR DEL SELECTOR ========================= \\

	$("#rangeAnnio").click(function(e){
		e.preventDefault();
		var annio = $("#annio").val();

		if(annio != ''){
			get_pagos_periodos('annio',annio,annio)
		}
	})


	// ===== FUNCION QUE CARGA LOS DATOS POR DEFAULT DE LOS PAGOS ====== \\



var thisYear = (new Date()).getFullYear();    
var start = new Date("1/1/" + thisYear);
var defaultStart = moment(start.valueOf());

var meses3 = moment().subtract(2, 'month').startOf('month')



	function get_pagos(startDate=meses3.format('YYYY-MM-DD'),endDate=moment().endOf('month').format('YYYY-MM-DD'),tipo='general'){


		var url = '/api/v1/recaudacion-api?format=json';

	  	var conf = {
	          headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
	          params : {
	          	'startDate':startDate,
	          	'endDate':endDate,
	          	'tipo':tipo
	          }
	      };

	    axios.get(url,conf)
	    .then(function(response){

	    	var $data = response.data


	    	// ===== INSTANCIA DEL GRÁFICO ===== \\

	    	pieChartOne($data.tableBasic,'tipo_pago','recaudacion','chartdiv');

    	//========================== BEGIN TABLA BASICA ==========================\\
	       var tableA = new myTable();

	       	tableA.columns = ['tipo_pago','nro_servicios',
	                       'recaudacion']
	       	tableA.palabraClave = 'Registros'
	       	tableA.displayLength = 15
	       	tableA.order_by_columns = [1,'desc']

	       	jsonA = tableA.render_table($data.tableBasic)

	       	jsonA['columns'][1] = {'data':'nro_servicios','className':'text-center'}
	       	
	   
	       	jsonA['columnDefs'].push(
	                           {"render": function (data, type, row) {
	                               return '<b><i>'+ Capitalize(data) +'</i></b>'},
	                           "targets": [0]
	                           })
	   
	      	var groupColumn = [1];

	      	jsonA['columnDefs'].push(
	                          {
	                          "render": function (data, type, row) {
	                                  return '<b><i>' +SeparateNumber(data)+'</i></b>';
	                              },
	                          "targets": [1]
							  })

	      	jsonA['columnDefs'].push(
	                          {
	                          "render": function (data, type, row) {
	                                  return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
	                              },
	                          "targets": [2]
	                          })


		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroA").val();
		    //       var dt_annio = data[0];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroB").val();
		    //       var dt_annio = data[1];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroC").val();
		    //       var dt_annio = data[2];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

			jsonA["footerCallback"] =  function ( row, data, start, end, display ) {
	               var api = this.api(), data;
	    
	               // Remove the formatting to get integer data for summation
	               var intVal = function ( i ) {
	                   return typeof i === 'string' ?
	                       i.replace(/[\$,]/g, '')*1 :
	                       typeof i === 'number' ?
	                           i : 0;
	               };
	   
	               size = api
	                   .column( 1 )
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );
	              
	                      
	               sizeDetail = api
	                   .column( 1 ,{page:'current'})
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
					   }, 0 );
					   
	    
	               // Total over all pages
	               totalDetail = api
	                   .column( 2, {page:'current'})
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );

	                total = api
	                   .column( 2 )
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );
	    
	               // Update footer
	               $( api.column( 1 ).footer() ).html(
	                   '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
				   );


	               $( api.column( 2 ).footer() ).html(
	                   '<b>$ '+ SeparateNumber(totalDetail) + ' | $ ' +SeparateNumber(total) + 
	                   '</b>'
	               );
	           }

	      	var bt = $("#example").DataTable(jsonA)
			// $("#filtroC,#filtroB,#filtroA").change(function(){
	  //           bt.draw()
	  //         })

		//========================== END TABLA BASICA ==========================\\




	    })
	    .then(function(){

	    })
	    .catch(function(rror){
	      handleErrorAxios(error)
	    })


	}

	// ===== FUNCION QUE CARGA LOS DATOS POR PERIODOS DE LOS PAGOS ====== \\

	function get_pagos_periodos(tipo='trimestre',annio,periodo){


		var url = '/api/v1/recaudacion-api?format=json';

	  	var conf = {
	          headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
	          params : {
	          	'annio':annio,
	          	'tipo':tipo,
	          	'periodo':periodo
	          }
	      };

	    axios.get(url,conf)
	    .then(function(response){

	    	var $data = response.data


	    	// ===== INSTANCIA DEL GRÁFICO ===== \\
	    	if(tipo == 'trimestre'){
	    		pieChartOne($data.tableBasic,'tipo_pago','recaudacion','chartdiv3');
	    		var id_tableBasic = "#example3"
	    		var id_tableDetail = "#example4"
	    	}else if(tipo == 'semestre'){
	    		pieChartOne($data.tableBasic,'tipo_pago','recaudacion','chartdiv5');
	    		var id_tableBasic = "#example5"
	    		var id_tableDetail = "#example6"
	    	}else if(tipo == 'annio'){
	    		pieChartOne($data.tableBasic,'tipo_pago','recaudacion','chartdiv7');
	    		var id_tableBasic = "#example7"
	    		var id_tableDetail = "#example8"
	    	}
	    	

    	//========================== BEGIN TABLA BASICA ==========================\\
	       var tableA = new myTable();

	       	tableA.columns = ['tipo_pago','nro_servicios',
	                       'recaudacion']
	       	tableA.palabraClave = 'Registros'
	       	tableA.displayLength = 15
	       	tableA.order_by_columns = [1,'desc']

	       	jsonA = tableA.render_table($data.tableBasic)

	       	jsonA['columns'][1] = {'data':'nro_servicios','className':'text-center'}
	       	
	   
	       	jsonA['columnDefs'].push(
	                           {"render": function (data, type, row) {
	                               return '<b><i>'+ Capitalize(data) +'</i></b>'},
	                           "targets": [0]
	                           })
	   
	      	var groupColumn = [1];

	      	jsonA['columnDefs'].push(
	                          {
	                          "render": function (data, type, row) {
	                                  return '<b><i>' +SeparateNumber(data)+'</i></b>';
	                              },
	                          "targets": [1]
							  })

	      	jsonA['columnDefs'].push(
	                          {
	                          "render": function (data, type, row) {
	                                  return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
	                              },
	                          "targets": [2]
	                          })


		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroA").val();
		    //       var dt_annio = data[0];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroB").val();
		    //       var dt_annio = data[1];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

		    // $.fn.dataTable.ext.search.push(
		    //   function( settings, data, dataIndex ) {
		    //       if ( settings.nTable.id !==  'example') {
		    //       return true;
		    //       }
		    //       var $select_dia = $("#filtroC").val();
		    //       var dt_annio = data[2];

		    //       if (( Capitalize($select_dia) == dt_annio ) || ( $select_dia == 'todos'))
		    //       {
		    //         return true;
		    //       }

		    //       return false;
		    //     }
		    // );

			jsonA["footerCallback"] =  function ( row, data, start, end, display ) {
	               var api = this.api(), data;
	    
	               // Remove the formatting to get integer data for summation
	               var intVal = function ( i ) {
	                   return typeof i === 'string' ?
	                       i.replace(/[\$,]/g, '')*1 :
	                       typeof i === 'number' ?
	                           i : 0;
	               };
	   
	               size = api
	                   .column( 1 )
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );
	              
	                      
	               sizeDetail = api
	                   .column( 1 ,{page:'current'})
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
					   }, 0 );
					   
	    
	               // Total over all pages
	               totalDetail = api
	                   .column( 2, {page:'current'})
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );

	                total = api
	                   .column( 2 )
	                   .data()
	                   .reduce( function (a, b) {
	                       return intVal(a) + intVal(b);
	                   }, 0 );
	    
	               // Update footer
	               $( api.column( 1 ).footer() ).html(
	                   '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
				   );


	               $( api.column( 2 ).footer() ).html(
	                   '<b>$ '+ SeparateNumber(totalDetail) + ' | $ ' +SeparateNumber(total) + 
	                   '</b>'
	               );
	           }

	      	var bt = $(id_tableBasic).DataTable(jsonA)
			// $("#filtroC,#filtroB,#filtroA").change(function(){
	  //           bt.draw()
	  //         })

		//========================== END TABLA BASICA ==========================\\




	    })
	    .then(function(){

	    })
	    .catch(function(error){
	      handleErrorAxios(error)
	    })


	}

	$(document).ready(function(){

		$.when(
			get_pagos(),
			$("#collapseOne,#collapseOne2,#collapseOne3,#collapseOne4").removeClass('show')
		)
		.then(function(){
			//=========== CONTENEDOR PPAL ===========\\

			$("#container_ppal").removeClass('filtros');
		})




		//===== BTN RESET'S ===== \\
		$("#resetDateOne").click(function(){
			
		    $('#startDate,#endDate').val("").datepicker("update");
		})

		$("#resetDateTrim").click(function(){
			
		    $('#annio_trimestre').val("").datepicker("update");
		    $("#trimestre").val(0).trigger('change');
		})

		$("#resetDateSem").click(function(){
			
		    $('#annio_semestre').val("").datepicker("update");
		    $("#semestre").val(0).trigger('change');
		})

		$("#resetDateYear").click(function(){
			
		    $('#annio').val("").datepicker("update");
		})


	});//============== FIN DE JQUERY ===================\\



	//pieChartOne(data,categoria,valor,div)

