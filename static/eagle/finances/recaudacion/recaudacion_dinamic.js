userInSession();
App.setPageTitle('Águila | Finanzas');
titlePage('Finanzas - Recaudación')

var json = {}

$(document).ready(function(){

	$.when(
	
		dateRange("#default-daterange"),
	
	)
	.then(function(){
		$("#container_ppal").removeClass('filtros')
	})

})


function dateRange(id){
    var start = moment().subtract(29, 'days');
    var end = moment();

    var quarter = moment().quarter();
    // Aquí se inicializa el selector de fechas
    $(id + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    //===== TRIMESTRES =====\\
    /******** FORMATO AAAA-MM-DD ********/
    var Q1A = moment().year().toString() + '-01-01'
    var Q1B = moment().year().toString() + '-03-31'

    var Q2A = moment().year().toString() + '-04-01'
    var Q2B = moment().year().toString() + '-06-30'

    var Q3A = moment().year().toString() + '-07-01'
    var Q3B = moment().year().toString() + '-09-30'

    var Q4A = moment().year().toString() + '-10-01'
    var Q4B = moment().year().toString() + '-12-31'

    //===== SEMESTRE =====\\

    var S1A = moment().year().toString() + '-01-01'
    var S1B = moment().year().toString() + '-06-30'

    var S2A = moment().year().toString() + '-07-01'
    var S2B = moment().year().toString() + '-12-31'



	var thisYear = (new Date()).getFullYear();    
	var start = new Date("1/1/" + thisYear);
	var defaultStart = moment(start.valueOf());




    $(id).daterangepicker({
        startDate: start,
        endDate: end,
        showDropdowns: true,
        minDate: '01/01/2001',
        maxDate: '01/01/2099',
        opens: 'right',
		drops: 'down',
		buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-warning',
        cancelClass: 'btn-inverse',
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Enviar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Seleccionar Fecha',
            todayLabel: 'Hoy',
            daysOfWeek: [
            "Lun",
            "Mar",
            "Mié",
            "Jue",
            "Vie",
            "Sáb",
            "Dom"],
            monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
            ],
            firstDay: 0
        },
        ranges: {
        'Mes actual': [moment().startOf('month'), moment().endOf('month')],

        'Trimestre 1': [moment(Q1A,'YYYY-MM-DD'), moment(Q1B,'YYYY-MM-DD')],
        'Trimestre 2': [moment(Q2A,'YYYY-MM-DD'), moment(Q2B,'YYYY-MM-DD')],
        'Trimestre 3': [moment(Q3A,'YYYY-MM-DD'), moment(Q3B,'YYYY-MM-DD')],
        'Trimestre 4': [moment(Q4A,'YYYY-MM-DD'), moment(Q4B,'YYYY-MM-DD')],

        'Semestre 1': [moment(S1A,'YYYY-MM-DD'), moment(S1B,'YYYY-MM-DD')],
        'Semestre 2': [moment(S2A,'YYYY-MM-DD'), moment(S2B,'YYYY-MM-DD')],

        'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Año anterior': [moment().subtract(364, 'days'), moment()],//
        'Año actual' : [defaultStart,moment()]
        }
        });

    //===== PARAMETROS PARA PETICIONES AJAX O AXIOS =====\\

    $(id).on('apply.daterangepicker', function(ev, picker) {
        /*
            MANEJADOR DE EVENTOS PROPIOS DEL DETERANGE, INICIA 
            CUANDO PULSAMOS EL BTN ACEPTAR(APPLY) DEL CALENDARIO
            DESPLEGADO.
        */

        ev.preventDefault()

        $("#chartdiv2").empty();
        $("#chartdiv2").empty().addClass('filtros')

        $("#card2,#tab2,#tab3").addClass('filtros');
        $("#tab_grafico").trigger('click');

        $("#startDate").val(picker.startDate.format('YYYY-MM-DD'))
        $("#endDate").val(picker.endDate.format('YYYY-MM-DD'))

        var params = {
            startDate:picker.startDate.format('YYYY-MM-DD'),
            endDate:picker.endDate.format('YYYY-MM-DD'),
            } 

        pagos_por_fecha(params)//PETICION AL SERVER

    })

}

function pagos_por_fecha(params){
 //===== 1ERA PETICIÓN AXIOS =====\\
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
        params:params,
    };
    //axios.get('api/v1/selector-api?format=json',conf)
    axios.get('api/v1/recaudacion-api?format=json',conf)
    .then(function(response){
    	var $data = response.data

    	if($data.results === true){

    		json['chartLine'] = $data.tableDetail

    		

    		$.when(
    			pieChartOne($data.tableBasic,'tipo_pago','recaudacion','chartdiv'),
    			
	    		$("#filtroA").select2({
	    			data:$data.filtroA
	    		}),

	    		$("#filtroB").select2({
	    			data:$data.filtroB
	    		}),

	    		tableBasic($data),
	    		tableDetail($data)


			)
			.then(function(){

				$("#container").removeClass('filtros');
				$("#collapseOne").removeClass('show');
				$("#card2,#tab2,#tab3").removeClass('filtros');
				$("#collapseTwo").addClass('show');

			})

    	}else{
    		_info($data.errorMsj)
    	}


    })
    .then(function(){

    })
    .catch(function(error){
    	handleErrorAxios(error)
    })
}




function tableBasic(data){

	var $data = data
//========================== BEGIN TABLA BASICA ==========================\\
   var tableA = new myTable();

   	tableA.columns = ['tipo_pago',
   					'nro_servicios',
   					'recaudacion',
   					'botones',
   					]
   	tableA.palabraClave = 'Registros'
   	tableA.order_by_columns = [2,'desc']

   	jsonA = tableA.render_table($data.tableBasic)
   	
    
    jsonA['columns'][1] = {'data':'nro_servicios','className':'text-center'}
   	jsonA['columns'][2] = {'data':'recaudacion','className':'text-center'}

   	jsonA['columnDefs'].push({
   						"render": function (data, type, row) {
                           return '<b><i>'+data +'</i></b>'
                       },
                       "targets": [0]
                       })

  	jsonA['columnDefs'].push(
                      {"render": function (data, type, row) {
                              return '<b><i>' +SeparateNumber(data)+'</i></b>';
                      },
                      "targets": [1]
					  })
					  
  	jsonA['columnDefs'].push(
                      {
                      "render": function (data, type, row) {
                              return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                          },
                      "targets": [2]
                      })


    jsonA["footerCallback"] =  function ( row, data, start, end, display ) {
       var api = this.api(), data;

       // Remove the formatting to get integer data for summation
       var intVal = function ( i ) {
           return typeof i === 'string' ?
               i.replace(/[\$,]/g, '')*1 :
               typeof i === 'number' ?
                   i : 0;
       };

       size = api
           .column( 1 )
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );
      
              
       sizeDetail = api
           .column( 1 ,{page:'current'})
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

       // Total over all pages
       totalDetail = api
           .column( 2, {page:'current'})
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

        total = api
           .column( 2 )
           .data()
           .reduce( function (a, b) {
               return intVal(a) + intVal(b);
           }, 0 );

       // Update footer
       $( api.column( 1 ).footer() ).html(
           '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
       );
       $( api.column( 2 ).footer() ).html(
           '<b>$'+ SeparateNumber(totalDetail) + ' | $ ' + SeparateNumber(total) + 
           '</b>'
       );
   }

    $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          if ( settings.nTable.id !==  'example') {
          return true;
          }
          var $select_dia = $("#filtroA").val();
          var dt_annio = data[0];

          if (($select_dia == dt_annio ) || ( $select_dia == 'todos'))
          {
            return true;
          }

          return false;
        });

  	var bt = $("#example").DataTable(jsonA)
	$("#filtroA").change(function(){
        bt.draw()
      })

//========================== END TABLA BASICA ==========================\\


}


function tableDetail(data){

	var $data = data	
//========================== BEGIN TABLA CLIENTES ==========================\\

    var tableB = new myTable();

    tableB.columns = ['tipo_pago',
            'cliente_name',
            'cliente_rut',
            'comuna',
            'id',
   					'nro_servicios',
   					'recaudacion',
   					]
    tableB.palabraClave = 'Clientes'
    tableB.order_by_columns = [0,'asc'],[4,'asc'],[6,'desc']
    jsonB = tableB.render_table($data.tableDetail)

    jsonB['columns'][2] = {'data':'cliente_rut','className':'text-center'}
    jsonB['columns'][3] = {'data':'comuna','className':'text-center'}
    jsonB['columns'][4] = {'data':'id','className':'text-center'}
    jsonB['columns'][5] = {'data':'nro_servicios','className':'text-center'}
    jsonB['columns'][6] = {'data':'recaudacion','className':'text-center'}

    jsonB['columnDefs'].push(
                           {'visible':false,
                           'searchable':true,
                           'orderable':true,
                           "targets": [0],
                           })    


    jsonB['columnDefs'].push(
                           {"render": function (data, type, row) {
                               return '<b><i>'+ data +'</i></b>'},
                           "targets": [1,2,3,4]
                           })	       

    jsonB['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>' + SeparateNumber(data) + '</i></b>';
                              },
                          "targets": [5]
                          })
    jsonB['columnDefs'].push(
                          {
                          "render": function (data, type, row) {
                                  return '<b><i>$ ' + SeparateNumber(data) + '</i></b>';
                              },
                          "targets": [6]
                          })

    jsonB["footerCallback"] =  function ( row, data, start, end, display ) {
               var api = this.api(), data;
    
               // Remove the formatting to get integer data for summation
               var intVal = function ( i ) {
                   return typeof i === 'string' ?
                       i.replace(/[\$,]/g, '')*1 :
                       typeof i === 'number' ?
                           i : 0;
               };
   
               size = api
                   .column( 5 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
              
                      
               sizeDetail = api
                   .column( 5 ,{page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
    
               // Total over all pages
               totalDetail = api
                   .column( 6, {page:'current'})
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );

                total = api
                   .column( 6 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
    
               // Update footer
               $( api.column( 5 ).footer() ).html(
                   '<b>'+ SeparateNumber(sizeDetail)+ ' | ' + SeparateNumber(size) + '</b>'
               );
               $( api.column( 6 ).footer() ).html(
                   '<b>$'+ SeparateNumber(totalDetail) + ' | $ ' + SeparateNumber(total) + 
                   '</b>'
               );
           }

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroA").val();
      var dt_annio = data[0];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );

  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      if ( settings.nTable.id !==  'example2') {
      return true;
      }
      var $select_dia = $("#filtroB").val();
      var dt_annio = data[4];

      if (( $select_dia == dt_annio ) || ( $select_dia == 'todos'))
      {
        return true;
      }

      return false;
    }
  );



    var groupColumn = [0];

    jsonB["drawCallback"] = function ( settings ) {
        var api = this.api();
        var rows = api.rows( {page:'current'} ).nodes();
        var last=null; 
        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
            if ( last !== group ) {
                $(rows).eq( i ).before(
                    '<tr class="group text-nowrap"><td colspan="7" style="color:#3b5998"><b><i>'+group.toUpperCase()+'</i></b></td></tr>'
                );

                last = group;
            }
        } );
       }


  	var dt = $("#example2").DataTable(jsonB)

  	$("#filtroA,#filtroB").change(function(){
	    dt.draw()
    })

//========================== END TABLA CLIENTES ==========================\\


}


function filtro_chart(obj){

	$("#chartdiv2").empty().removeClass('filtros')


	var $a = json['chartLine'].filter(word => word.tipo_pago === obj.tipo_pago)

	$("#endpoint").val(obj.tipo_pago)

	GraficoFechas($a,'fecha_pago','chartdiv2')

}


