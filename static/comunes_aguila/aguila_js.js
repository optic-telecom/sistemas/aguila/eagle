//* localStorage *//
var HDD = {
    get: function(key) {
        return localStorage.getItem(key);
    },
    get_default: function(key, value_default) {
    	try{
    		return localStorage.getItem(key);
    	}
    	catch(err){
    		return value_default;
    	}
    },
    set: function(key, val) {
        return localStorage.setItem(key, val);
    },
    unset: function(key) {
        return localStorage.removeItem(key);
    },
    setJ:function(key,val)
    {
        return localStorage.setItem(key, JSON.stringify(val));
    },
    getJ:function(key)
    {
        return JSON.parse(localStorage.getItem(key));
    }
};


function DjangoURL(url,id='',version='v1') {
  return Django[url].replace(':val:',id).replace('v1',version);
}


//* session *//
function sessionExit(redirect=true) {

    HDD.unset('username');
    HDD.unset(Django.name_jwt);
    
/*    for (var i = 0; i < localStorage.length; i++) {
        HDD.unset(localStorage.key(i)); 
    }

    for(var x = 0; x <= localStorage.length; x++) {
        localStorage.removeItem(localStorage.key(x))
    }*/

    Cookies.remove(Django.name_jwt)

	if (redirect)
	{
		setTimeout(function(){ location.href = '/'; }, 40);
	}
};

function invalidToken() {
    HDD.set('last_url',window.location.pathname);
    sessionExit(true);
};


function handleErrorAxios(error) {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        // console.log(error.response.data);

        if (error.response.status == 400)
        {
          if (error.response.data.error){
            _error(""+ error.response.data.error).then(function(){});
          }else{
            _error("Por favor verifique los datos ingresados.");
          }
        }
        else if (error.response.status == 401)
        {
          handleUnauthorizedAxios();
        }
        else if (error.response.status == 404)
        {
          console.log("error 404 ", error.request.responseURL)
        }
        else if(error.response.status == 500){
            _error('Hubo un error en el sistema. Contacte al administrador.')
            .then(function() {});
        }        
        else{
          console.info("Response:");
          console.log(error.response);
          // console.log(error.response.headers);
        }

    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.info("Request");
        console.log(error.request);
        console.log(error)
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error message: ', error.message);
        console.log('Error stack: ', error.stack);
    }
    //console.log(error.config);
}

function handleUnauthorizedAxios() {
  _error_redirect("Se vencio su session, porfavor ingrese nuevamente",
  '/','Sesion expirada');
}

function handleTooManyRequestsAxios()
{
  console.log("Too many requests ");
}

function verifyJwtToken() {
  axios.post('api/v1/token/verify_jwt_token/', {token: HDD.get(Django.name_jwt)})
  .then(function(response){
    if (response.status != 200)
    {
      sessionExit(true);
    }
  })
  .catch(function(error) {
      sessionExit(true);
  });    
}

function refreshJwtToken() {
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
    };

    axios.post('api/v1/token/refresh/', {token: HDD.get(Django.name_jwt)}, conf)
    .then(function(response){
      if (response.status == 200)
      {
        HDD.set(Django.name_jwt,response.data.token);
        Cookies.set(Django.name_jwt, response.data.token);
      }
    })
    .catch(function(error) {
        sessionExit(true);
    });

}

function userInSession(redirect=true) {

    if (HDD.get(Django.name_jwt) == null)
    {
      sessionExit(redirect);
      return;
    }

    if (HDD.get('username') == null)
    {
      var conf = {
          headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
      };
  
      axios.get(Django.api_user, conf)
      .then(function(response){
        if (response.status == 200)
        {
          HDD.set('username', response.data.username);
          $(".text_username").text(Capitalize(HDD.get('username')));
          $(".fecha").text(Django.fecha)
        }
      })
      .catch(function(error) {
          handleErrorAxios(error);
      });

    }
    else
    {
      $(".text_username").text(Capitalize(HDD.get('username')));
      $(".fecha").html('<b>Fecha</b>: '+Django.fecha)
    }
    refreshJwtToken();
}


function titlePage(name){
  $(".page-header").html(Django.app_name + ' | <small>' + name + '</small>')
  return false;
}

//================== alerts sweetalert ================== //
function _info(textMessage) {
    swal({
        title: "",
        text: textMessage,
        icon: "info",
        buttons: false,
    });
};

function _error(textMessage) {
    noti = swal({
        title: "Error",
        text: textMessage,
        icon: "error",
        buttons: false,
    });
    return noti
};

function _error_redirect(textMessage,redirect,title='Error') {
    swal({  
        title: title,
        text: textMessage,
        icon: "error",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
        })
    .then(function() {
        location.href = redirect;
    });
};

function _exito(textMessage) {
	swal({
	title: "Información",
		text: textMessage,
		icon: "success",
        buttons: false
	});
};

function _exito_redirect(textMessage,dire) {
	swal({
	title: "Información",
		text: textMessage,
		icon: "success",
    buttons: false
	}).then(function() {
		location.href = dire;
	});
};

function _confirma(textMessage, yes, not) {

  swal({
      title: "Información",
      text: textMessage,
      icon: "warning",
      buttons: [
        'No',
        'Si'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        yes()
      } else {
        not()
      }
    })

}



//* events *//
$("#btn_log_out").on('click',function(){
    sessionExit(true);
})

