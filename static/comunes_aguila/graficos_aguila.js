//Métodos para renderizar Gráficos

//=========== GRAFICOS ===============//

//keepCount = true variable para recargar graficos
function pieChartOne(data,categoria,valor,div="chartdiv"){

	

	am4core.ready(function() {
		keepCount = true;
	// Themes begin
	am4core.useTheme(am4themes_material);
	am4core.useTheme(am4themes_animated);
	// Themes end

	// Create chart instance
	var chart = am4core.create(div, am4charts.PieChart);

	chart.titles.template.fontSize = 20;
	chart.titles.create().text = "Gráfico Donut";

	// Add data
	chart.data = data;

	chart.language.locale = am4lang_es_ES;
	chart.numberFormatter.language = new am4core.Language();
	chart.numberFormatter.language.locale = am4lang_es_ES;
	chart.dateFormatter.language = new am4core.Language();
	chart.dateFormatter.language.locale = am4lang_es_ES;

	chart.language.locale["_decimalSeparator"] = ",";
	chart.language.locale["_thousandSeparator"] = "."; 

	// Set inner radius
	chart.innerRadius = am4core.percent(50);

	// Add and configure Series
	var pieSeries = chart.series.push(new am4charts.PieSeries());
	pieSeries.dataFields.value = "nro_servicios";
	pieSeries.dataFields.category = categoria;
	pieSeries.slices.template.stroke = am4core.color("#fff");
	pieSeries.slices.template.strokeWidth = 2;
	pieSeries.slices.template.strokeOpacity = 0.8;

	// This creates initial animation
	pieSeries.hiddenState.properties.opacity = 0.8;
	pieSeries.hiddenState.properties.endAngle = -90;
	pieSeries.hiddenState.properties.startAngle = -90;


	// Disabling labels and ticks on inner circle
	pieSeries.labels.template.disabled = true;
	pieSeries.ticks.template.disabled = true;

	// Disable sliding out of slices
	pieSeries.slices.template.states.getKey("hover").properties.shiftRadius = 0;
	pieSeries.slices.template.states.getKey("hover").properties.scale = 0.9;


	// Add second series
	var pieSeries2 = chart.series.push(new am4charts.PieSeries());
	//pieSeries2.hidden = true
	pieSeries2.dataFields.value = valor;
	pieSeries2.dataFields.category = categoria;
	pieSeries2.slices.template.stroke = am4core.color("#fff");
	pieSeries2.slices.template.strokeWidth = 2;
	pieSeries2.slices.template.strokeOpacity = 1;
	pieSeries2.slices.template.states.getKey("hover").properties.shiftRadius = 0;
	pieSeries2.slices.template.states.getKey("hover").properties.scale = 1.1;

	  // Add legend
  	//chart.legend = new am4charts.Legend();


	}); // end am4core.ready()
}


//=========== GRAFICOS CHORD ===============//

function ChordGraph(data,div="chartdiv"){

	

	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_material);
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create(div, am4charts.ChordDiagram);

	// colors of main characters
	chart.colors.saturation = 0.45;
	chart.colors.step = 3;
	var colors = {
	    Rachel:chart.colors.next(),
	    Monica:chart.colors.next(),
	    Phoebe:chart.colors.next(),
	    Ross:chart.colors.next(),
	    Joey:chart.colors.next(),
	    Chandler:chart.colors.next()
	}

	// data was provided by: https://www.reddit.com/user/notrudedude

	chart.data = data;
	chart.dataFields.fromName = "from";
	chart.dataFields.toName = "to";
	chart.dataFields.value = "value";
	chart.responsive.enable = true

	chart.nodePadding = 0.5;
	chart.minNodeSize = 0.01;
	chart.startAngle = 80;
	chart.endAngle = chart.startAngle + 360;
	chart.sortBy = "value";
	chart.fontSize = 10;

	var nodeTemplate = chart.nodes.template;
	nodeTemplate.readerTitle = "Click para mostrar/ocultar este dato";
	nodeTemplate.showSystemTooltip = true;
	nodeTemplate.propertyFields.fill = "color";
	nodeTemplate.tooltipText = "{name}'s kisses: {total}";

	// when rolled over the node, make all the links rolled-over
	nodeTemplate.events.on("over", function(event) {    
	    var node = event.target;
	    node.outgoingDataItems.each(function(dataItem) {
	        if(dataItem.toNode){
	            dataItem.link.isHover = true;
	            dataItem.toNode.label.isHover = true;
	        }
	    })
	    node.incomingDataItems.each(function(dataItem) {
	        if(dataItem.fromNode){
	            dataItem.link.isHover = true;
	            dataItem.fromNode.label.isHover = true;
	        }
	    }) 

	    node.label.isHover = true;   
	})

	// when rolled out from the node, make all the links rolled-out
	nodeTemplate.events.on("out", function(event) {
	    var node = event.target;
	    node.outgoingDataItems.each(function(dataItem) {        
	        if(dataItem.toNode){
	            dataItem.link.isHover = false;                
	            dataItem.toNode.label.isHover = false;
	        }
	    })
	    node.incomingDataItems.each(function(dataItem) {
	        if(dataItem.fromNode){
	            dataItem.link.isHover = false;
	           dataItem.fromNode.label.isHover = false;
	        }
	    })

	    node.label.isHover = false;
	})

	var label = nodeTemplate.label;
	label.relativeRotation = 90;

	label.fillOpacity = 0.4;
	let labelHS = label.states.create("hover");
	labelHS.properties.fillOpacity = 1;

	nodeTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer;
	// this adapter makes non-main character nodes to be filled with color of the main character which he/she kissed most
	nodeTemplate.adapter.add("fill", function(fill, target) {
	    let node = target;
	    let counters = {};
	    let mainChar = false;
	    node.incomingDataItems.each(function(dataItem) {
	        if(colors[dataItem.toName]){
	            mainChar = true;
	        }

	        if(isNaN(counters[dataItem.fromName])){
	            counters[dataItem.fromName] = dataItem.value;
	        }
	        else{
	            counters[dataItem.fromName] += dataItem.value;
	        }
	    })
	    if(mainChar){
	        return fill;
	    }

	    let count = 0;
	    let color;
	    let biggest = 0;
	    let biggestName;

	    for(var name in counters){
	        if(counters[name] > biggest){
	            biggestName = name;
	            biggest = counters[name]; 
	        }        
	    }
	    if(colors[biggestName]){
	        fill = colors[biggestName];
	    }
	  
	    return fill;
	})

	// link template
	var linkTemplate = chart.links.template;
	linkTemplate.strokeOpacity = 0;
	linkTemplate.fillOpacity = 0.15;
	linkTemplate.tooltipText = "{fromName} -- {toName}:{value.value} servicios";

	var hoverState = linkTemplate.states.create("hover");
	hoverState.properties.fillOpacity = 0.7;
	hoverState.properties.strokeOpacity = 0.7;

	// data credit label
	var creditLabel = chart.chartContainer.createChild(am4core.TextLink);
	creditLabel.text = "Datos proporcionados por Optic cl";
	creditLabel.url = "https://www.optic.cl";
	creditLabel.y = am4core.percent(99);
	creditLabel.x = am4core.percent(99);
	creditLabel.horizontalCenter = "right";
	creditLabel.verticalCenter = "bottom";

	var titleImage = chart.chartContainer.createChild(am4core.Image);
	//titleImage.href = "//www.amcharts.com/wp-content/uploads/2018/11/whokissed.png";
	titleImage.href = "/static/icons/multifiberMediano.png";
	titleImage.x = 15
	titleImage.y = -5;
	titleImage.width = 160;
	titleImage.height = 160;

	}); // end am4core.ready()
}

function ChordGraph2(data,div='chartdiv'){
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		
		
		var chart = am4core.create(div, am4charts.ChordDiagram);
		
		
		chart.data = data;
		
		chart.dataFields.fromName = "from";
		chart.dataFields.toName = "to";
		chart.dataFields.value = "value";
		
		// make nodes draggable
		var nodeTemplate = chart.nodes.template;
		nodeTemplate.readerTitle = "Click to show/hide or drag to rearrange";
		nodeTemplate.showSystemTooltip = true;
		
		var nodeLink = chart.links.template;
		var bullet = nodeLink.bullets.push(new am4charts.CircleBullet());
		bullet.fillOpacity = 1;
		bullet.circle.radius = 5;
		bullet.locationX = 0.5;
		
		// create animations
		chart.events.on("ready", function() {
			for (var i = 0; i < chart.links.length; i++) {
				var link = chart.links.getIndex(i);
				var bullet = link.bullets.getIndex(0);
		
				animateBullet(bullet);
			}
		})
		
		function animateBullet(bullet) {
			var duration = 3000 * Math.random() + 2000;
			var animation = bullet.animate([{ property: "locationX", from: 0, to: 1 }], duration)
			animation.events.on("animationended", function(event) {
				animateBullet(event.target.object);
			})
		}
		
		}); // end am4core.ready()
}


//=========== GRAFICOS NODOS ===============//

function nodeGraph(data){
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		var chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);
		var networkSeries = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries())
		
		chart.data = [
		  {
			name: "Core",
			children: [
			  {
				name: "First",
				children: [
				  { name: "A1", value: 100 },
				  { name: "A2", value: 60 }
				]
			  },
			  {
				name: "Second",
				children: [
				  { name: "B1", value: 135 },
				  { name: "B2", value: 98 }
				]
			  },
			  {
				name: "Third",
				children: [
				  {
					name: "C1",
					children: [
					  { name: "EE1", value: 130 },
					  { name: "EE2", value: 87 },
					  { name: "EE3", value: 55 }
					]
				  },
				  { name: "C2", value: 148 },
				  {
					name: "C3", children: [
					  { name: "CC1", value: 53 },
					  { name: "CC2", value: 30 }
					]
				  },
				  { name: "C4", value: 26 }
				]
			  },
			  {
				name: "Fourth",
				children: [
				  { name: "D1", value: 415 },
				  { name: "D2", value: 148 },
				  { name: "D3", value: 89 }
				]
			  },
			  {
				name: "Fifth",
				children: [
				  {
					name: "E1",
					children: [
					  { name: "EE1", value: 33 },
					  { name: "EE2", value: 40 },
					  { name: "EE3", value: 89 }
					]
				  },
				  {
					name: "E2",
					value: 148
				  }
				]
			  }
		
			]
		  }
		];
		
		networkSeries.dataFields.value = "value";
		networkSeries.dataFields.name = "name";
		networkSeries.dataFields.children = "children";
		networkSeries.nodes.template.tooltipText = "{name}:{value}";
		networkSeries.nodes.template.fillOpacity = 1;
		networkSeries.manyBodyStrength = -20;
		networkSeries.links.template.strength = 0.8;
		networkSeries.minRadius = am4core.percent(2);
		
		networkSeries.nodes.template.label.text = "{name}"
		networkSeries.fontSize = 10;
		
		}); // end am4core.ready()
}


//=========== GRAFICOS SANKEY ===============//

function SankeyGraph(data,div='chartdiv'){

	am4core.ready(function() {



	// Themes begin
	am4core.useTheme(am4themes_material);
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create(div, am4charts.SankeyDiagram);
	chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

	chart.language.locale = am4lang_es_ES;
	chart.numberFormatter.language = new am4core.Language();
	chart.numberFormatter.language.locale = am4lang_es_ES;
	chart.dateFormatter.language = new am4core.Language();
	chart.dateFormatter.language.locale = am4lang_es_ES;

	chart.language.locale["_decimalSeparator"] = ",";
	chart.language.locale["_thousandSeparator"] = "."; 


	//Darle formato al gráfico y mostrar el nombre completo de la etiqueta
	chart.nodes.template.nameLabel.label.truncate = false;
	chart.nodes.template.nameLabel.label.wrap = true;

	chart.data = [];// AQUI VACIAMOS LA DATA ANTIGUA

	chart.data = data; //aqui cargamos la data nueva

/*	let hoverState = chart.links.template.states.create("hover");
	hoverState.properties.fillOpacity = 0.6;*/

	chart.dataFields.fromName = "from";
	chart.dataFields.toName = "to";
	chart.dataFields.value = "value";
	chart.padding(0, 100, 10, 0);

/**/

	// Add title
	chart.titles.template.fontSize = 20;
	chart.titles.create().text = "Gráfico Dinámico de Servicios";


	chart.links.template.tooltipText = "[bold]{fromName} → {toName}: {value2} servicios para una recaudacion de $ {value}.[/]";

	var hoverState = chart.links.template.states.create("hover");
	hoverState.properties.fillOpacity = 1;

	// for right-most label to fit
	chart.paddingRight = 100;

	// make nodes draggable
	var nodeTemplate = chart.nodes.template;
	nodeTemplate.inert = true;
	nodeTemplate.readerTitle = "Drag me!";
	nodeTemplate.showSystemTooltip = true;
	nodeTemplate.width = 20;

	// make nodes draggable
	var nodeTemplate = chart.nodes.template;
	nodeTemplate.readerTitle = "Arrastre para reorganizar o haga clic para mostrar y ocultar";
	nodeTemplate.showSystemTooltip = true;
	nodeTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer

	}); // end am4core.ready()


}


function diagramaSankey(data, div='chartdiv'){


	am4core.ready(function() {


keepCount = true
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create(div, am4charts.SankeyDiagram);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = data;


//Darle formato al gráfico y mostrar el nombre completo de la etiqueta
chart.nodes.template.nameLabel.label.truncate = false;
chart.nodes.template.nameLabel.label.wrap = true;

chart.language.locale = am4lang_es_ES;
chart.numberFormatter.language = new am4core.Language();
chart.numberFormatter.language.locale = am4lang_es_ES;
chart.dateFormatter.language = new am4core.Language();
chart.dateFormatter.language.locale = am4lang_es_ES;

chart.language.locale["_decimalSeparator"] = ",";
chart.language.locale["_thousandSeparator"] = "."; 

let hoverState = chart.links.template.states.create("hover");
hoverState.properties.fillOpacity = 0.6;

chart.dataFields.fromName = "from";
chart.dataFields.toName = "to";
chart.dataFields.value = "value";

chart.links.template.propertyFields.id = "id";
chart.links.template.colorMode = "solid";
chart.links.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
chart.links.template.fillOpacity = 0.1;
// chart.links.template.tooltipText = "";

// highlight all links with the same id beginning
chart.links.template.events.on("over", function(event){
  let link = event.target;
  let id = link.id.split("-")[0];

  chart.links.each(function(link){
    if(link.id.indexOf(id) != -1){
      link.isHover = true;
    }
  })
})

chart.links.template.events.on("out", function(event){  
  chart.links.each(function(link){
    link.isHover = false;
  })
})

// for right-most label to fit
chart.paddingRight = 40;

  // make nodes draggable
  var nodeTemplate = chart.nodes.template;
  nodeTemplate.draggable = true;
  nodeTemplate.inert = true;
  nodeTemplate.readerTitle = "Arrastre para reorganizar o haga clic para mostrar y ocultar";
  nodeTemplate.showSystemTooltip = true;
  nodeTemplate.width = 40;
  nodeTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer

// make nodes draggable
// var nodeTemplate = chart.nodes.template;
// nodeTemplate.readerTitle = "Click para mostrar u ocultar las secciones del gráfico";
// nodeTemplate.showSystemTooltip = true;
// nodeTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer

}); // end am4core.ready()
}

function barrasModal(data,categoria,div='chartdiv'){
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_material);
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		// Create chart instance
		var chart = am4core.create(div, am4charts.XYChart);
		chart.scrollbarX = new am4core.Scrollbar();
		
		// Add data
		chart.data = data;
		
		// Create axes
		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = categoria;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 30;
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.renderer.labels.template.verticalCenter = "middle";
		categoryAxis.renderer.labels.template.rotation = 270;
		categoryAxis.tooltip.disabled = true;
		categoryAxis.renderer.minHeight = 110;
		
		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.minWidth = 50;
		
		// Create series
		var series = chart.series.push(new am4charts.ColumnSeries());
		series.sequencedInterpolation = true;
		series.dataFields.valueY = "nro_servicios";
		series.dataFields.categoryX = categoria;
		series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
		series.columns.template.strokeWidth = 0;
		
		series.tooltip.pointerOrientation = "vertical";
		
		series.columns.template.column.cornerRadiusTopLeft = 10;
		series.columns.template.column.cornerRadiusTopRight = 10;
		series.columns.template.column.fillOpacity = 0.8;
		
		// on hover, make corner radiuses bigger
		var hoverState = series.columns.template.column.states.create("hover");
		hoverState.properties.cornerRadiusTopLeft = 0;
		hoverState.properties.cornerRadiusTopRight = 0;
		hoverState.properties.fillOpacity = 1;
		
		series.columns.template.adapter.add("fill", function(fill, target) {
		  return chart.colors.getIndex(target.dataItem.index);
		});
		
		// Cursor
		chart.cursor = new am4charts.XYCursor();
		
		}); // end am4core.ready()
}

function circuloBasico(data, categoria, div="chartdiv"){

	

	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_material);
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		// Create chart instance
		var chart = am4core.create(div, am4charts.PieChart);
		
		// Add data
		chart.data = data;
		chart.language.locale = am4lang_es_ES;
		chart.numberFormatter.language = new am4core.Language();
		chart.numberFormatter.language.locale = am4lang_es_ES;
		chart.dateFormatter.language = new am4core.Language();
		chart.dateFormatter.language.locale = am4lang_es_ES;
	  
		chart.language.locale["_decimalSeparator"] = ",";
		chart.language.locale["_thousandSeparator"] = "."; 
		
		// Set inner radius
		chart.innerRadius = am4core.percent(50);
		
		// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "recaudacion";
		pieSeries.dataFields.category = categoria;
		pieSeries.slices.template.stroke = am4core.color("#fff");
		pieSeries.slices.template.strokeWidth = 2;
		pieSeries.slices.template.strokeOpacity = 1;
		
		// This creates initial animation
		pieSeries.hiddenState.properties.opacity = 1;
		pieSeries.hiddenState.properties.endAngle = -90;
		pieSeries.hiddenState.properties.startAngle = -90;
		
		}); // end am4core.ready()
}

function semi_circulo(data,categoria,div='chartdiv'){
	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_material);
		am4core.useTheme(am4themes_animated);
		// Themes end
		
		var chart = am4core.create(div, am4charts.PieChart);
		chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
		
		chart.data = data;
		chart.radius = am4core.percent(70);
		chart.innerRadius = am4core.percent(40);
		chart.startAngle = 180;
		chart.endAngle = 360;  
		
		var series = chart.series.push(new am4charts.PieSeries());
		series.dataFields.value = "nro_servicios";
		series.dataFields.category = categoria;
		
		series.slices.template.cornerRadius = 10;
		series.slices.template.innerCornerRadius = 7;
		series.slices.template.draggable = true;
		series.slices.template.inert = true;
		series.alignLabels = false;
		
		series.hiddenState.properties.startAngle = 90;
		series.hiddenState.properties.endAngle = 90;
		
		chart.legend = new am4charts.Legend();
		
		});
}



/*OJO CON ESTE GRAFICO*/
function GraficoComparativo(data,div='chartdiv'){

	am4core.useTheme(am4themes_animated);
	am4core.useTheme(am4themes_material);

	// Create chart instance
	var chart = am4core.create(div, am4charts.XYChart);

	// Add data
	/*chart.data = [
	  { "date": new Date(2014, 0, 1), "value": 450 },
	];*/

	data = data;

	// Create axes
	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.grid.template.location = 0.5;
	dateAxis.renderer.labels.template.location = 0.5;
	dateAxis.renderer.minGridDistance = 30;
	dateAxis.dateFormats.setKey("month", "[font-size: 12px]MM");
	dateAxis.periodChangeDateFormats.setKey("month", "[bold]yyyy");

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

	// Create series
	function createSeries(field, name) {
	  var series = chart.series.push(new am4charts.ColumnSeries());
	  series.dataFields.valueY = field; 
	  series.dataFields.dateX = "date";
	  series.name = name;   
	  series.tooltipText = "{dateX}: [b]{valueY}[/]";
	  series.strokeWidth = 2;
	}

	createSeries("value", "Series #1");

	chart.cursor = new am4charts.XYCursor();
	chart.scrollbarX = new am4core.Scrollbar();
}


function GraficoFechas2(data,categoria,div='chartdiv'){

  am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  am4core.useTheme(am4themes_material);
  // Themes end

  // Create chart instance
  var chart = am4core.create(div, am4charts.XYChart);

  //chart.numberFormatter.numberFormat = "#,###";
  chart.language.locale = am4lang_es_ES;
  chart.numberFormatter.language = new am4core.Language();
  chart.numberFormatter.language.locale = am4lang_es_ES;
  chart.dateFormatter.language = new am4core.Language();
  chart.dateFormatter.language.locale = am4lang_es_ES;

  chart.language.locale["_decimalSeparator"] = ",";
  chart.language.locale["_thousandSeparator"] = "."; 

  chart.responsive.enable=true;

  // Create daily series and related axes
  var dateAxis1 = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis1.renderer.grid.template.location = 0;
  dateAxis1.renderer.minGridDistance = 40;

  var valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());

  var series2 = chart.series.push(new am4charts.LineSeries());
  series2.dataFields.valueY = "recaudacion";
  series2.dataFields.dateX = categoria;
  series2.name = "Recaudación"
  series2.data = data;
  series2.xAxis = dateAxis1;
  series2.yAxis = valueAxis1;
  series2.strokeWidth = 3;
  series2.tooltipText = "Fecha: {dateX.formatDate('yyyy-MM-dd')}\n[bold]Recaudacion: {valueY.formatNumber('#.')}[/]";


  var series4 = chart.series.push(new am4charts.LineSeries());
  series4.dataFields.valueY = "nro_servicios";
  series4.dataFields.dateX = categoria;
  series4.data = data;
  series4.name = "Nro de servicios"
  series4.xAxis = dateAxis1;
  series4.yAxis = valueAxis1;
  series4.strokeWidth = 3;
  series4.hidden = true
  series4.tooltipText = "Fecha: {dateX.formatDate('yyyy-MM-dd')}\n[bold]Nro de servicios: {valueY.formatNumber('#.')}[/]";


  // Add cursor
  chart.cursor = new am4charts.XYCursor();

  var circleBullet = series2.bullets.push(new am4charts.CircleBullet());
  circleBullet.circle.fill = am4core.color("#fb8c00");
  circleBullet.circle.strokeWidth = 2;


  var circleBullet3 = series4.bullets.push(new am4charts.CircleBullet());
  circleBullet3.circle.fill = am4core.color("#000000");
  circleBullet3.circle.strokeWidth = 2;

  // Add legend
  chart.legend = new am4charts.Legend();

  }); // end am4core.ready()
}

function GraficoFechas(data,categoria,div='chartdiv'){

	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_material);
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create(div, am4charts.XYChart);
	chart.paddingRight = 20;

	chart.language.locale = am4lang_es_ES;
	chart.numberFormatter.language = new am4core.Language();
	chart.numberFormatter.language.locale = am4lang_es_ES;
	chart.dateFormatter.language = new am4core.Language();
	chart.dateFormatter.language.locale = am4lang_es_ES;

	chart.language.locale["_decimalSeparator"] = ",";
	chart.language.locale["_thousandSeparator"] = "."; 

	chart.responsive.enable=true;

	chart.data = data;

	tipo_graph = $("#endpoint option:selected").text();

	if(!tipo_graph){
		tipo_graph = 'Servicios'
	}

	chart.titles.template.fontSize = 20;
	chart.titles.create().text = "Gráfico Lineal de " + tipo_graph;

	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.grid.template.location = 0;
	dateAxis.renderer.axisFills.template.disabled = true;
	dateAxis.renderer.ticks.template.disabled = true;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.tooltip.disabled = true;
	valueAxis.renderer.minWidth = 35;
	valueAxis.renderer.axisFills.template.disabled = true;
	valueAxis.renderer.ticks.template.disabled = true;



	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.dateX = categoria;
	series.dataFields.valueY = "recaudacion";
	series.dataFields.valueZ = "nro_servicios";
	series.strokeWidth = 2;
	series.tooltipText = "[bold]Recaudación[/]: $ {valueY}\n[bold]Nro de "+ tipo_graph +"[/]: {valueZ},\n [bold]Fecha[/]: {dateX.formatDate('dd-MM-yyyy')}";

	var circleBullet = series.bullets.push(new am4charts.CircleBullet());
	circleBullet.circle.fill = am4core.color("#000000");
	circleBullet.circle.strokeWidth = 2;

	chart.cursor = new am4charts.XYCursor();


	// Create vertical scrollbar and place it before the value axis
	chart.scrollbarY = new am4core.Scrollbar();

	// Create a horizontal scrollbar with previe and place it underneath the date axis
	chart.scrollbarX = new am4charts.XYChartScrollbar();
	chart.scrollbarX.series.push(series);
	chart.scrollbarX.parent = chart.bottomAxesContainer;

	chart.events.on("ready", function(ev) {
	  dateAxis.zoomToDates(
	    chart.data[0].fecha_pago,
	    chart.data[5].fecha_pago
	  );
	});

	}); // end am4core.ready()

}