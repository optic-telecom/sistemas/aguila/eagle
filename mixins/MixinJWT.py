import jwt
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import PermissionDenied
from django.utils.six import wraps, string_types
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import resolve_url
from django.utils.six.moves.urllib.parse import urlparse
from django.conf import settings


from django.views.generic import TemplateView
from rest_framework_jwt.settings import api_settings
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
User = get_user_model()



def has_permission(user, perm_name):
    try:
        if user.is_superuser:
            return True
    except:        
        return False

    return user.get_permission(perm_name)


def has_permission_403(user, perm):
    if not has_permission(user, perm):
        # return HttpResponseForbidden()
        raise PermissionDenied


def redirect(request):
    path = request.build_absolute_uri()
    resolved_login_url = resolve_url(settings.LOGIN_URL)
    # If the login url is the same scheme and net location then just
    # use the path as the "next" url.
    login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
    current_scheme, current_netloc = urlparse(path)[:2]
    if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
        path = request.get_full_path()

    return redirect_to_login(
    path, resolved_login_url, REDIRECT_FIELD_NAME)


class LoginWithPermMixin(AccessMixin):

    """
        Mixin para validar algun permiso del usuario,
        en algun template
    """
    permission = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            try:
                return self.without_permission()
            except AttributeError:
                return redirect()

        if self.permission:
            if isinstance(self.permission, string_types):
                has_permission_403(request.user, self.permission)
            else:
                for perm_itsem in self.permission:
                    has_permission_403(request.user, self.permission)

        return super(LoginWithPermMixin, self).dispatch(request, *args, **kwargs)


class JWTMixin(AccessMixin):

    jwt = settings.NAME_JWT

    def validate(self, request):
        if self.jwt in request.COOKIES:
            try:
                jwt = request.COOKIES[self.jwt]
                decode = jwt_decode_handler(jwt)

                user = User.objects.get(username=decode['username'])
                return True
            except Exception as e:
                print (e)
                return False

        return False
        
    def dispatch(self, request, *args, **kwargs):
        if not self.validate(request):
            response = redirect(request)
            if self.jwt in request.COOKIES:
                del request.COOKIES[self.jwt]
                response.delete_cookie(self.jwt)
            return response
        return super().dispatch(request, *args, **kwargs)


class JWTView(JWTMixin,TemplateView):
    pass